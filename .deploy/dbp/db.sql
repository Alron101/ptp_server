-- mydatabase
create table places
(
  photo_name             varchar(512),
  description            varchar(512),
  id                     integer,
  latitude               numeric,
  longitude              numeric,
  country                varchar(512),
  city                   varchar(512),
  vk_link                varchar(512),
  user_vk_id             integer,
  file_name_in_filesytem varchar(512)
);


create table employees
(
    first_name    varchar(512),
    last_name     varchar(512),
    email_address varchar(512),
    id            bigserial not null
        constraint employees_pk
            primary key,
user_vk_id    serial    not null
);
