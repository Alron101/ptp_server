package ru.petthepet.server.web.rest;

import ru.petthepet.server.PtpApp;
import ru.petthepet.server.domain.Shelter;
import ru.petthepet.server.domain.Pet;
import ru.petthepet.server.repository.ShelterRepository;
import ru.petthepet.server.repository.search.ShelterSearchRepository;
import ru.petthepet.server.service.ShelterService;
import ru.petthepet.server.service.dto.ShelterDTO;
import ru.petthepet.server.service.mapper.ShelterMapper;
import ru.petthepet.server.web.rest.errors.ExceptionTranslator;
import ru.petthepet.server.service.dto.ShelterCriteria;
import ru.petthepet.server.service.ShelterQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static ru.petthepet.server.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ShelterResource} REST controller.
 */
@SpringBootTest(classes = PtpApp.class)
public class ShelterResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LINK = "AAAAAAAAAA";
    private static final String UPDATED_LINK = "BBBBBBBBBB";

    private static final Integer DEFAULT_GROUP_ID = 1;
    private static final Integer UPDATED_GROUP_ID = 2;
    private static final Integer SMALLER_GROUP_ID = 1 - 1;

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_REG_INFO = "AAAAAAAAAA";
    private static final String UPDATED_REG_INFO = "BBBBBBBBBB";

    private static final Boolean DEFAULT_NEED_FEED = false;
    private static final Boolean UPDATED_NEED_FEED = true;

    private static final Boolean DEFAULT_NEED_HANDS = false;
    private static final Boolean UPDATED_NEED_HANDS = true;

    private static final Boolean DEFAULT_NEED_GROOMING = false;
    private static final Boolean UPDATED_NEED_GROOMING = true;

    private static final Boolean DEFAULT_NEED_MED_HELP = false;
    private static final Boolean UPDATED_NEED_MED_HELP = true;

    private static final Boolean DEFAULT_NEED_WALK = false;
    private static final Boolean UPDATED_NEED_WALK = true;

    private static final Boolean DEFAULT_NEED_OVEREXPOSURE = false;
    private static final Boolean UPDATED_NEED_OVEREXPOSURE = true;

    private static final Boolean DEFAULT_NEED_GROUP_DESIGN_HELP = false;
    private static final Boolean UPDATED_NEED_GROUP_DESIGN_HELP = true;

    private static final Boolean DEFAULT_NEED_REPAIR = false;
    private static final Boolean UPDATED_NEED_REPAIR = true;

    private static final Boolean DEFAULT_NEED_TRANSPORT = false;
    private static final Boolean UPDATED_NEED_TRANSPORT = true;

    private static final Boolean DEFAULT_CAN_PHOTO_SESSION = false;
    private static final Boolean UPDATED_CAN_PHOTO_SESSION = true;

    private static final Boolean DEFAULT_CAN_VISIT = false;
    private static final Boolean UPDATED_CAN_VISIT = true;

    private static final String DEFAULT_LOAD_ALBUMS = "AAAAAAAAAA";
    private static final String UPDATED_LOAD_ALBUMS = "BBBBBBBBBB";

    private static final String DEFAULT_LOAD_PHOTO_FROM_ALBUMS = "AAAAAAAAAA";
    private static final String UPDATED_LOAD_PHOTO_FROM_ALBUMS = "BBBBBBBBBB";

    private static final Boolean DEFAULT_LOAD_BY_TAGS = false;
    private static final Boolean UPDATED_LOAD_BY_TAGS = true;

    @Autowired
    private ShelterRepository shelterRepository;

    @Autowired
    private ShelterMapper shelterMapper;

    @Autowired
    private ShelterService shelterService;

    /**
     * This repository is mocked in the ru.petthepet.server.repository.search test package.
     *
     * @see ru.petthepet.server.repository.search.ShelterSearchRepositoryMockConfiguration
     */
    @Autowired
    private ShelterSearchRepository mockShelterSearchRepository;

    @Autowired
    private ShelterQueryService shelterQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restShelterMockMvc;

    private Shelter shelter;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ShelterResource shelterResource = new ShelterResource(shelterService, shelterQueryService);
        this.restShelterMockMvc = MockMvcBuilders.standaloneSetup(shelterResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Shelter createEntity(EntityManager em) {
        Shelter shelter = new Shelter()
            .name(DEFAULT_NAME)
            .link(DEFAULT_LINK)
            .groupId(DEFAULT_GROUP_ID)
            .city(DEFAULT_CITY)
            .description(DEFAULT_DESCRIPTION)
            .regInfo(DEFAULT_REG_INFO)
            .needFeed(DEFAULT_NEED_FEED)
            .needHands(DEFAULT_NEED_HANDS)
            .needGrooming(DEFAULT_NEED_GROOMING)
            .needMedHelp(DEFAULT_NEED_MED_HELP)
            .needWalk(DEFAULT_NEED_WALK)
            .needOverexposure(DEFAULT_NEED_OVEREXPOSURE)
            .needGroupDesignHelp(DEFAULT_NEED_GROUP_DESIGN_HELP)
            .needRepair(DEFAULT_NEED_REPAIR)
            .needTransport(DEFAULT_NEED_TRANSPORT)
            .canPhotoSession(DEFAULT_CAN_PHOTO_SESSION)
            .canVisit(DEFAULT_CAN_VISIT)
            .loadAlbums(DEFAULT_LOAD_ALBUMS)
            .loadPhotoFromAlbums(DEFAULT_LOAD_PHOTO_FROM_ALBUMS)
            .loadByTags(DEFAULT_LOAD_BY_TAGS);
        return shelter;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Shelter createUpdatedEntity(EntityManager em) {
        Shelter shelter = new Shelter()
            .name(UPDATED_NAME)
            .link(UPDATED_LINK)
            .groupId(UPDATED_GROUP_ID)
            .city(UPDATED_CITY)
            .description(UPDATED_DESCRIPTION)
            .regInfo(UPDATED_REG_INFO)
            .needFeed(UPDATED_NEED_FEED)
            .needHands(UPDATED_NEED_HANDS)
            .needGrooming(UPDATED_NEED_GROOMING)
            .needMedHelp(UPDATED_NEED_MED_HELP)
            .needWalk(UPDATED_NEED_WALK)
            .needOverexposure(UPDATED_NEED_OVEREXPOSURE)
            .needGroupDesignHelp(UPDATED_NEED_GROUP_DESIGN_HELP)
            .needRepair(UPDATED_NEED_REPAIR)
            .needTransport(UPDATED_NEED_TRANSPORT)
            .canPhotoSession(UPDATED_CAN_PHOTO_SESSION)
            .canVisit(UPDATED_CAN_VISIT)
            .loadAlbums(UPDATED_LOAD_ALBUMS)
            .loadPhotoFromAlbums(UPDATED_LOAD_PHOTO_FROM_ALBUMS)
            .loadByTags(UPDATED_LOAD_BY_TAGS);
        return shelter;
    }

    @BeforeEach
    public void initTest() {
        shelter = createEntity(em);
    }

    @Test
    @Transactional
    public void createShelter() throws Exception {
        int databaseSizeBeforeCreate = shelterRepository.findAll().size();

        // Create the Shelter
        ShelterDTO shelterDTO = shelterMapper.toDto(shelter);
        restShelterMockMvc.perform(post("/api/shelters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shelterDTO)))
            .andExpect(status().isCreated());

        // Validate the Shelter in the database
        List<Shelter> shelterList = shelterRepository.findAll();
        assertThat(shelterList).hasSize(databaseSizeBeforeCreate + 1);
        Shelter testShelter = shelterList.get(shelterList.size() - 1);
        assertThat(testShelter.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testShelter.getLink()).isEqualTo(DEFAULT_LINK);
        assertThat(testShelter.getGroupId()).isEqualTo(DEFAULT_GROUP_ID);
        assertThat(testShelter.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testShelter.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testShelter.getRegInfo()).isEqualTo(DEFAULT_REG_INFO);
        assertThat(testShelter.isNeedFeed()).isEqualTo(DEFAULT_NEED_FEED);
        assertThat(testShelter.isNeedHands()).isEqualTo(DEFAULT_NEED_HANDS);
        assertThat(testShelter.isNeedGrooming()).isEqualTo(DEFAULT_NEED_GROOMING);
        assertThat(testShelter.isNeedMedHelp()).isEqualTo(DEFAULT_NEED_MED_HELP);
        assertThat(testShelter.isNeedWalk()).isEqualTo(DEFAULT_NEED_WALK);
        assertThat(testShelter.isNeedOverexposure()).isEqualTo(DEFAULT_NEED_OVEREXPOSURE);
        assertThat(testShelter.isNeedGroupDesignHelp()).isEqualTo(DEFAULT_NEED_GROUP_DESIGN_HELP);
        assertThat(testShelter.isNeedRepair()).isEqualTo(DEFAULT_NEED_REPAIR);
        assertThat(testShelter.isNeedTransport()).isEqualTo(DEFAULT_NEED_TRANSPORT);
        assertThat(testShelter.isCanPhotoSession()).isEqualTo(DEFAULT_CAN_PHOTO_SESSION);
        assertThat(testShelter.isCanVisit()).isEqualTo(DEFAULT_CAN_VISIT);
        assertThat(testShelter.getLoadAlbums()).isEqualTo(DEFAULT_LOAD_ALBUMS);
        assertThat(testShelter.getLoadPhotoFromAlbums()).isEqualTo(DEFAULT_LOAD_PHOTO_FROM_ALBUMS);
        assertThat(testShelter.isLoadByTags()).isEqualTo(DEFAULT_LOAD_BY_TAGS);

        // Validate the Shelter in Elasticsearch
        verify(mockShelterSearchRepository, times(1)).save(testShelter);
    }

    @Test
    @Transactional
    public void createShelterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = shelterRepository.findAll().size();

        // Create the Shelter with an existing ID
        shelter.setId(1L);
        ShelterDTO shelterDTO = shelterMapper.toDto(shelter);

        // An entity with an existing ID cannot be created, so this API call must fail
        restShelterMockMvc.perform(post("/api/shelters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shelterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Shelter in the database
        List<Shelter> shelterList = shelterRepository.findAll();
        assertThat(shelterList).hasSize(databaseSizeBeforeCreate);

        // Validate the Shelter in Elasticsearch
        verify(mockShelterSearchRepository, times(0)).save(shelter);
    }


    @Test
    @Transactional
    public void getAllShelters() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList
        restShelterMockMvc.perform(get("/api/shelters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shelter.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].link").value(hasItem(DEFAULT_LINK.toString())))
            .andExpect(jsonPath("$.[*].groupId").value(hasItem(DEFAULT_GROUP_ID)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].regInfo").value(hasItem(DEFAULT_REG_INFO.toString())))
            .andExpect(jsonPath("$.[*].needFeed").value(hasItem(DEFAULT_NEED_FEED.booleanValue())))
            .andExpect(jsonPath("$.[*].needHands").value(hasItem(DEFAULT_NEED_HANDS.booleanValue())))
            .andExpect(jsonPath("$.[*].needGrooming").value(hasItem(DEFAULT_NEED_GROOMING.booleanValue())))
            .andExpect(jsonPath("$.[*].needMedHelp").value(hasItem(DEFAULT_NEED_MED_HELP.booleanValue())))
            .andExpect(jsonPath("$.[*].needWalk").value(hasItem(DEFAULT_NEED_WALK.booleanValue())))
            .andExpect(jsonPath("$.[*].needOverexposure").value(hasItem(DEFAULT_NEED_OVEREXPOSURE.booleanValue())))
            .andExpect(jsonPath("$.[*].needGroupDesignHelp").value(hasItem(DEFAULT_NEED_GROUP_DESIGN_HELP.booleanValue())))
            .andExpect(jsonPath("$.[*].needRepair").value(hasItem(DEFAULT_NEED_REPAIR.booleanValue())))
            .andExpect(jsonPath("$.[*].needTransport").value(hasItem(DEFAULT_NEED_TRANSPORT.booleanValue())))
            .andExpect(jsonPath("$.[*].canPhotoSession").value(hasItem(DEFAULT_CAN_PHOTO_SESSION.booleanValue())))
            .andExpect(jsonPath("$.[*].canVisit").value(hasItem(DEFAULT_CAN_VISIT.booleanValue())))
            .andExpect(jsonPath("$.[*].loadAlbums").value(hasItem(DEFAULT_LOAD_ALBUMS.toString())))
            .andExpect(jsonPath("$.[*].loadPhotoFromAlbums").value(hasItem(DEFAULT_LOAD_PHOTO_FROM_ALBUMS.toString())))
            .andExpect(jsonPath("$.[*].loadByTags").value(hasItem(DEFAULT_LOAD_BY_TAGS.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getShelter() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get the shelter
        restShelterMockMvc.perform(get("/api/shelters/{id}", shelter.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(shelter.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.link").value(DEFAULT_LINK.toString()))
            .andExpect(jsonPath("$.groupId").value(DEFAULT_GROUP_ID))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.regInfo").value(DEFAULT_REG_INFO.toString()))
            .andExpect(jsonPath("$.needFeed").value(DEFAULT_NEED_FEED.booleanValue()))
            .andExpect(jsonPath("$.needHands").value(DEFAULT_NEED_HANDS.booleanValue()))
            .andExpect(jsonPath("$.needGrooming").value(DEFAULT_NEED_GROOMING.booleanValue()))
            .andExpect(jsonPath("$.needMedHelp").value(DEFAULT_NEED_MED_HELP.booleanValue()))
            .andExpect(jsonPath("$.needWalk").value(DEFAULT_NEED_WALK.booleanValue()))
            .andExpect(jsonPath("$.needOverexposure").value(DEFAULT_NEED_OVEREXPOSURE.booleanValue()))
            .andExpect(jsonPath("$.needGroupDesignHelp").value(DEFAULT_NEED_GROUP_DESIGN_HELP.booleanValue()))
            .andExpect(jsonPath("$.needRepair").value(DEFAULT_NEED_REPAIR.booleanValue()))
            .andExpect(jsonPath("$.needTransport").value(DEFAULT_NEED_TRANSPORT.booleanValue()))
            .andExpect(jsonPath("$.canPhotoSession").value(DEFAULT_CAN_PHOTO_SESSION.booleanValue()))
            .andExpect(jsonPath("$.canVisit").value(DEFAULT_CAN_VISIT.booleanValue()))
            .andExpect(jsonPath("$.loadAlbums").value(DEFAULT_LOAD_ALBUMS.toString()))
            .andExpect(jsonPath("$.loadPhotoFromAlbums").value(DEFAULT_LOAD_PHOTO_FROM_ALBUMS.toString()))
            .andExpect(jsonPath("$.loadByTags").value(DEFAULT_LOAD_BY_TAGS.booleanValue()));
    }

    @Test
    @Transactional
    public void getAllSheltersByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where name equals to DEFAULT_NAME
        defaultShelterShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the shelterList where name equals to UPDATED_NAME
        defaultShelterShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllSheltersByNameIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where name in DEFAULT_NAME or UPDATED_NAME
        defaultShelterShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the shelterList where name equals to UPDATED_NAME
        defaultShelterShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllSheltersByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where name is not null
        defaultShelterShouldBeFound("name.specified=true");

        // Get all the shelterList where name is null
        defaultShelterShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByLinkIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where link equals to DEFAULT_LINK
        defaultShelterShouldBeFound("link.equals=" + DEFAULT_LINK);

        // Get all the shelterList where link equals to UPDATED_LINK
        defaultShelterShouldNotBeFound("link.equals=" + UPDATED_LINK);
    }

    @Test
    @Transactional
    public void getAllSheltersByLinkIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where link in DEFAULT_LINK or UPDATED_LINK
        defaultShelterShouldBeFound("link.in=" + DEFAULT_LINK + "," + UPDATED_LINK);

        // Get all the shelterList where link equals to UPDATED_LINK
        defaultShelterShouldNotBeFound("link.in=" + UPDATED_LINK);
    }

    @Test
    @Transactional
    public void getAllSheltersByLinkIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where link is not null
        defaultShelterShouldBeFound("link.specified=true");

        // Get all the shelterList where link is null
        defaultShelterShouldNotBeFound("link.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByGroupIdIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where groupId equals to DEFAULT_GROUP_ID
        defaultShelterShouldBeFound("groupId.equals=" + DEFAULT_GROUP_ID);

        // Get all the shelterList where groupId equals to UPDATED_GROUP_ID
        defaultShelterShouldNotBeFound("groupId.equals=" + UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    public void getAllSheltersByGroupIdIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where groupId in DEFAULT_GROUP_ID or UPDATED_GROUP_ID
        defaultShelterShouldBeFound("groupId.in=" + DEFAULT_GROUP_ID + "," + UPDATED_GROUP_ID);

        // Get all the shelterList where groupId equals to UPDATED_GROUP_ID
        defaultShelterShouldNotBeFound("groupId.in=" + UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    public void getAllSheltersByGroupIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where groupId is not null
        defaultShelterShouldBeFound("groupId.specified=true");

        // Get all the shelterList where groupId is null
        defaultShelterShouldNotBeFound("groupId.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByGroupIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where groupId is greater than or equal to DEFAULT_GROUP_ID
        defaultShelterShouldBeFound("groupId.greaterThanOrEqual=" + DEFAULT_GROUP_ID);

        // Get all the shelterList where groupId is greater than or equal to UPDATED_GROUP_ID
        defaultShelterShouldNotBeFound("groupId.greaterThanOrEqual=" + UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    public void getAllSheltersByGroupIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where groupId is less than or equal to DEFAULT_GROUP_ID
        defaultShelterShouldBeFound("groupId.lessThanOrEqual=" + DEFAULT_GROUP_ID);

        // Get all the shelterList where groupId is less than or equal to SMALLER_GROUP_ID
        defaultShelterShouldNotBeFound("groupId.lessThanOrEqual=" + SMALLER_GROUP_ID);
    }

    @Test
    @Transactional
    public void getAllSheltersByGroupIdIsLessThanSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where groupId is less than DEFAULT_GROUP_ID
        defaultShelterShouldNotBeFound("groupId.lessThan=" + DEFAULT_GROUP_ID);

        // Get all the shelterList where groupId is less than UPDATED_GROUP_ID
        defaultShelterShouldBeFound("groupId.lessThan=" + UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    public void getAllSheltersByGroupIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where groupId is greater than DEFAULT_GROUP_ID
        defaultShelterShouldNotBeFound("groupId.greaterThan=" + DEFAULT_GROUP_ID);

        // Get all the shelterList where groupId is greater than SMALLER_GROUP_ID
        defaultShelterShouldBeFound("groupId.greaterThan=" + SMALLER_GROUP_ID);
    }


    @Test
    @Transactional
    public void getAllSheltersByCityIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where city equals to DEFAULT_CITY
        defaultShelterShouldBeFound("city.equals=" + DEFAULT_CITY);

        // Get all the shelterList where city equals to UPDATED_CITY
        defaultShelterShouldNotBeFound("city.equals=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllSheltersByCityIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where city in DEFAULT_CITY or UPDATED_CITY
        defaultShelterShouldBeFound("city.in=" + DEFAULT_CITY + "," + UPDATED_CITY);

        // Get all the shelterList where city equals to UPDATED_CITY
        defaultShelterShouldNotBeFound("city.in=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllSheltersByCityIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where city is not null
        defaultShelterShouldBeFound("city.specified=true");

        // Get all the shelterList where city is null
        defaultShelterShouldNotBeFound("city.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where description equals to DEFAULT_DESCRIPTION
        defaultShelterShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the shelterList where description equals to UPDATED_DESCRIPTION
        defaultShelterShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllSheltersByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultShelterShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the shelterList where description equals to UPDATED_DESCRIPTION
        defaultShelterShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllSheltersByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where description is not null
        defaultShelterShouldBeFound("description.specified=true");

        // Get all the shelterList where description is null
        defaultShelterShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByRegInfoIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where regInfo equals to DEFAULT_REG_INFO
        defaultShelterShouldBeFound("regInfo.equals=" + DEFAULT_REG_INFO);

        // Get all the shelterList where regInfo equals to UPDATED_REG_INFO
        defaultShelterShouldNotBeFound("regInfo.equals=" + UPDATED_REG_INFO);
    }

    @Test
    @Transactional
    public void getAllSheltersByRegInfoIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where regInfo in DEFAULT_REG_INFO or UPDATED_REG_INFO
        defaultShelterShouldBeFound("regInfo.in=" + DEFAULT_REG_INFO + "," + UPDATED_REG_INFO);

        // Get all the shelterList where regInfo equals to UPDATED_REG_INFO
        defaultShelterShouldNotBeFound("regInfo.in=" + UPDATED_REG_INFO);
    }

    @Test
    @Transactional
    public void getAllSheltersByRegInfoIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where regInfo is not null
        defaultShelterShouldBeFound("regInfo.specified=true");

        // Get all the shelterList where regInfo is null
        defaultShelterShouldNotBeFound("regInfo.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedFeedIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needFeed equals to DEFAULT_NEED_FEED
        defaultShelterShouldBeFound("needFeed.equals=" + DEFAULT_NEED_FEED);

        // Get all the shelterList where needFeed equals to UPDATED_NEED_FEED
        defaultShelterShouldNotBeFound("needFeed.equals=" + UPDATED_NEED_FEED);
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedFeedIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needFeed in DEFAULT_NEED_FEED or UPDATED_NEED_FEED
        defaultShelterShouldBeFound("needFeed.in=" + DEFAULT_NEED_FEED + "," + UPDATED_NEED_FEED);

        // Get all the shelterList where needFeed equals to UPDATED_NEED_FEED
        defaultShelterShouldNotBeFound("needFeed.in=" + UPDATED_NEED_FEED);
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedFeedIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needFeed is not null
        defaultShelterShouldBeFound("needFeed.specified=true");

        // Get all the shelterList where needFeed is null
        defaultShelterShouldNotBeFound("needFeed.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedHandsIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needHands equals to DEFAULT_NEED_HANDS
        defaultShelterShouldBeFound("needHands.equals=" + DEFAULT_NEED_HANDS);

        // Get all the shelterList where needHands equals to UPDATED_NEED_HANDS
        defaultShelterShouldNotBeFound("needHands.equals=" + UPDATED_NEED_HANDS);
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedHandsIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needHands in DEFAULT_NEED_HANDS or UPDATED_NEED_HANDS
        defaultShelterShouldBeFound("needHands.in=" + DEFAULT_NEED_HANDS + "," + UPDATED_NEED_HANDS);

        // Get all the shelterList where needHands equals to UPDATED_NEED_HANDS
        defaultShelterShouldNotBeFound("needHands.in=" + UPDATED_NEED_HANDS);
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedHandsIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needHands is not null
        defaultShelterShouldBeFound("needHands.specified=true");

        // Get all the shelterList where needHands is null
        defaultShelterShouldNotBeFound("needHands.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedGroomingIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needGrooming equals to DEFAULT_NEED_GROOMING
        defaultShelterShouldBeFound("needGrooming.equals=" + DEFAULT_NEED_GROOMING);

        // Get all the shelterList where needGrooming equals to UPDATED_NEED_GROOMING
        defaultShelterShouldNotBeFound("needGrooming.equals=" + UPDATED_NEED_GROOMING);
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedGroomingIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needGrooming in DEFAULT_NEED_GROOMING or UPDATED_NEED_GROOMING
        defaultShelterShouldBeFound("needGrooming.in=" + DEFAULT_NEED_GROOMING + "," + UPDATED_NEED_GROOMING);

        // Get all the shelterList where needGrooming equals to UPDATED_NEED_GROOMING
        defaultShelterShouldNotBeFound("needGrooming.in=" + UPDATED_NEED_GROOMING);
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedGroomingIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needGrooming is not null
        defaultShelterShouldBeFound("needGrooming.specified=true");

        // Get all the shelterList where needGrooming is null
        defaultShelterShouldNotBeFound("needGrooming.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedMedHelpIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needMedHelp equals to DEFAULT_NEED_MED_HELP
        defaultShelterShouldBeFound("needMedHelp.equals=" + DEFAULT_NEED_MED_HELP);

        // Get all the shelterList where needMedHelp equals to UPDATED_NEED_MED_HELP
        defaultShelterShouldNotBeFound("needMedHelp.equals=" + UPDATED_NEED_MED_HELP);
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedMedHelpIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needMedHelp in DEFAULT_NEED_MED_HELP or UPDATED_NEED_MED_HELP
        defaultShelterShouldBeFound("needMedHelp.in=" + DEFAULT_NEED_MED_HELP + "," + UPDATED_NEED_MED_HELP);

        // Get all the shelterList where needMedHelp equals to UPDATED_NEED_MED_HELP
        defaultShelterShouldNotBeFound("needMedHelp.in=" + UPDATED_NEED_MED_HELP);
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedMedHelpIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needMedHelp is not null
        defaultShelterShouldBeFound("needMedHelp.specified=true");

        // Get all the shelterList where needMedHelp is null
        defaultShelterShouldNotBeFound("needMedHelp.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedWalkIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needWalk equals to DEFAULT_NEED_WALK
        defaultShelterShouldBeFound("needWalk.equals=" + DEFAULT_NEED_WALK);

        // Get all the shelterList where needWalk equals to UPDATED_NEED_WALK
        defaultShelterShouldNotBeFound("needWalk.equals=" + UPDATED_NEED_WALK);
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedWalkIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needWalk in DEFAULT_NEED_WALK or UPDATED_NEED_WALK
        defaultShelterShouldBeFound("needWalk.in=" + DEFAULT_NEED_WALK + "," + UPDATED_NEED_WALK);

        // Get all the shelterList where needWalk equals to UPDATED_NEED_WALK
        defaultShelterShouldNotBeFound("needWalk.in=" + UPDATED_NEED_WALK);
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedWalkIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needWalk is not null
        defaultShelterShouldBeFound("needWalk.specified=true");

        // Get all the shelterList where needWalk is null
        defaultShelterShouldNotBeFound("needWalk.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedOverexposureIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needOverexposure equals to DEFAULT_NEED_OVEREXPOSURE
        defaultShelterShouldBeFound("needOverexposure.equals=" + DEFAULT_NEED_OVEREXPOSURE);

        // Get all the shelterList where needOverexposure equals to UPDATED_NEED_OVEREXPOSURE
        defaultShelterShouldNotBeFound("needOverexposure.equals=" + UPDATED_NEED_OVEREXPOSURE);
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedOverexposureIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needOverexposure in DEFAULT_NEED_OVEREXPOSURE or UPDATED_NEED_OVEREXPOSURE
        defaultShelterShouldBeFound("needOverexposure.in=" + DEFAULT_NEED_OVEREXPOSURE + "," + UPDATED_NEED_OVEREXPOSURE);

        // Get all the shelterList where needOverexposure equals to UPDATED_NEED_OVEREXPOSURE
        defaultShelterShouldNotBeFound("needOverexposure.in=" + UPDATED_NEED_OVEREXPOSURE);
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedOverexposureIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needOverexposure is not null
        defaultShelterShouldBeFound("needOverexposure.specified=true");

        // Get all the shelterList where needOverexposure is null
        defaultShelterShouldNotBeFound("needOverexposure.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedGroupDesignHelpIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needGroupDesignHelp equals to DEFAULT_NEED_GROUP_DESIGN_HELP
        defaultShelterShouldBeFound("needGroupDesignHelp.equals=" + DEFAULT_NEED_GROUP_DESIGN_HELP);

        // Get all the shelterList where needGroupDesignHelp equals to UPDATED_NEED_GROUP_DESIGN_HELP
        defaultShelterShouldNotBeFound("needGroupDesignHelp.equals=" + UPDATED_NEED_GROUP_DESIGN_HELP);
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedGroupDesignHelpIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needGroupDesignHelp in DEFAULT_NEED_GROUP_DESIGN_HELP or UPDATED_NEED_GROUP_DESIGN_HELP
        defaultShelterShouldBeFound("needGroupDesignHelp.in=" + DEFAULT_NEED_GROUP_DESIGN_HELP + "," + UPDATED_NEED_GROUP_DESIGN_HELP);

        // Get all the shelterList where needGroupDesignHelp equals to UPDATED_NEED_GROUP_DESIGN_HELP
        defaultShelterShouldNotBeFound("needGroupDesignHelp.in=" + UPDATED_NEED_GROUP_DESIGN_HELP);
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedGroupDesignHelpIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needGroupDesignHelp is not null
        defaultShelterShouldBeFound("needGroupDesignHelp.specified=true");

        // Get all the shelterList where needGroupDesignHelp is null
        defaultShelterShouldNotBeFound("needGroupDesignHelp.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedRepairIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needRepair equals to DEFAULT_NEED_REPAIR
        defaultShelterShouldBeFound("needRepair.equals=" + DEFAULT_NEED_REPAIR);

        // Get all the shelterList where needRepair equals to UPDATED_NEED_REPAIR
        defaultShelterShouldNotBeFound("needRepair.equals=" + UPDATED_NEED_REPAIR);
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedRepairIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needRepair in DEFAULT_NEED_REPAIR or UPDATED_NEED_REPAIR
        defaultShelterShouldBeFound("needRepair.in=" + DEFAULT_NEED_REPAIR + "," + UPDATED_NEED_REPAIR);

        // Get all the shelterList where needRepair equals to UPDATED_NEED_REPAIR
        defaultShelterShouldNotBeFound("needRepair.in=" + UPDATED_NEED_REPAIR);
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedRepairIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needRepair is not null
        defaultShelterShouldBeFound("needRepair.specified=true");

        // Get all the shelterList where needRepair is null
        defaultShelterShouldNotBeFound("needRepair.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedTransportIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needTransport equals to DEFAULT_NEED_TRANSPORT
        defaultShelterShouldBeFound("needTransport.equals=" + DEFAULT_NEED_TRANSPORT);

        // Get all the shelterList where needTransport equals to UPDATED_NEED_TRANSPORT
        defaultShelterShouldNotBeFound("needTransport.equals=" + UPDATED_NEED_TRANSPORT);
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedTransportIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needTransport in DEFAULT_NEED_TRANSPORT or UPDATED_NEED_TRANSPORT
        defaultShelterShouldBeFound("needTransport.in=" + DEFAULT_NEED_TRANSPORT + "," + UPDATED_NEED_TRANSPORT);

        // Get all the shelterList where needTransport equals to UPDATED_NEED_TRANSPORT
        defaultShelterShouldNotBeFound("needTransport.in=" + UPDATED_NEED_TRANSPORT);
    }

    @Test
    @Transactional
    public void getAllSheltersByNeedTransportIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where needTransport is not null
        defaultShelterShouldBeFound("needTransport.specified=true");

        // Get all the shelterList where needTransport is null
        defaultShelterShouldNotBeFound("needTransport.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByCanPhotoSessionIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where canPhotoSession equals to DEFAULT_CAN_PHOTO_SESSION
        defaultShelterShouldBeFound("canPhotoSession.equals=" + DEFAULT_CAN_PHOTO_SESSION);

        // Get all the shelterList where canPhotoSession equals to UPDATED_CAN_PHOTO_SESSION
        defaultShelterShouldNotBeFound("canPhotoSession.equals=" + UPDATED_CAN_PHOTO_SESSION);
    }

    @Test
    @Transactional
    public void getAllSheltersByCanPhotoSessionIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where canPhotoSession in DEFAULT_CAN_PHOTO_SESSION or UPDATED_CAN_PHOTO_SESSION
        defaultShelterShouldBeFound("canPhotoSession.in=" + DEFAULT_CAN_PHOTO_SESSION + "," + UPDATED_CAN_PHOTO_SESSION);

        // Get all the shelterList where canPhotoSession equals to UPDATED_CAN_PHOTO_SESSION
        defaultShelterShouldNotBeFound("canPhotoSession.in=" + UPDATED_CAN_PHOTO_SESSION);
    }

    @Test
    @Transactional
    public void getAllSheltersByCanPhotoSessionIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where canPhotoSession is not null
        defaultShelterShouldBeFound("canPhotoSession.specified=true");

        // Get all the shelterList where canPhotoSession is null
        defaultShelterShouldNotBeFound("canPhotoSession.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByCanVisitIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where canVisit equals to DEFAULT_CAN_VISIT
        defaultShelterShouldBeFound("canVisit.equals=" + DEFAULT_CAN_VISIT);

        // Get all the shelterList where canVisit equals to UPDATED_CAN_VISIT
        defaultShelterShouldNotBeFound("canVisit.equals=" + UPDATED_CAN_VISIT);
    }

    @Test
    @Transactional
    public void getAllSheltersByCanVisitIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where canVisit in DEFAULT_CAN_VISIT or UPDATED_CAN_VISIT
        defaultShelterShouldBeFound("canVisit.in=" + DEFAULT_CAN_VISIT + "," + UPDATED_CAN_VISIT);

        // Get all the shelterList where canVisit equals to UPDATED_CAN_VISIT
        defaultShelterShouldNotBeFound("canVisit.in=" + UPDATED_CAN_VISIT);
    }

    @Test
    @Transactional
    public void getAllSheltersByCanVisitIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where canVisit is not null
        defaultShelterShouldBeFound("canVisit.specified=true");

        // Get all the shelterList where canVisit is null
        defaultShelterShouldNotBeFound("canVisit.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByLoadAlbumsIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where loadAlbums equals to DEFAULT_LOAD_ALBUMS
        defaultShelterShouldBeFound("loadAlbums.equals=" + DEFAULT_LOAD_ALBUMS);

        // Get all the shelterList where loadAlbums equals to UPDATED_LOAD_ALBUMS
        defaultShelterShouldNotBeFound("loadAlbums.equals=" + UPDATED_LOAD_ALBUMS);
    }

    @Test
    @Transactional
    public void getAllSheltersByLoadAlbumsIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where loadAlbums in DEFAULT_LOAD_ALBUMS or UPDATED_LOAD_ALBUMS
        defaultShelterShouldBeFound("loadAlbums.in=" + DEFAULT_LOAD_ALBUMS + "," + UPDATED_LOAD_ALBUMS);

        // Get all the shelterList where loadAlbums equals to UPDATED_LOAD_ALBUMS
        defaultShelterShouldNotBeFound("loadAlbums.in=" + UPDATED_LOAD_ALBUMS);
    }

    @Test
    @Transactional
    public void getAllSheltersByLoadAlbumsIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where loadAlbums is not null
        defaultShelterShouldBeFound("loadAlbums.specified=true");

        // Get all the shelterList where loadAlbums is null
        defaultShelterShouldNotBeFound("loadAlbums.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByLoadPhotoFromAlbumsIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where loadPhotoFromAlbums equals to DEFAULT_LOAD_PHOTO_FROM_ALBUMS
        defaultShelterShouldBeFound("loadPhotoFromAlbums.equals=" + DEFAULT_LOAD_PHOTO_FROM_ALBUMS);

        // Get all the shelterList where loadPhotoFromAlbums equals to UPDATED_LOAD_PHOTO_FROM_ALBUMS
        defaultShelterShouldNotBeFound("loadPhotoFromAlbums.equals=" + UPDATED_LOAD_PHOTO_FROM_ALBUMS);
    }

    @Test
    @Transactional
    public void getAllSheltersByLoadPhotoFromAlbumsIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where loadPhotoFromAlbums in DEFAULT_LOAD_PHOTO_FROM_ALBUMS or UPDATED_LOAD_PHOTO_FROM_ALBUMS
        defaultShelterShouldBeFound("loadPhotoFromAlbums.in=" + DEFAULT_LOAD_PHOTO_FROM_ALBUMS + "," + UPDATED_LOAD_PHOTO_FROM_ALBUMS);

        // Get all the shelterList where loadPhotoFromAlbums equals to UPDATED_LOAD_PHOTO_FROM_ALBUMS
        defaultShelterShouldNotBeFound("loadPhotoFromAlbums.in=" + UPDATED_LOAD_PHOTO_FROM_ALBUMS);
    }

    @Test
    @Transactional
    public void getAllSheltersByLoadPhotoFromAlbumsIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where loadPhotoFromAlbums is not null
        defaultShelterShouldBeFound("loadPhotoFromAlbums.specified=true");

        // Get all the shelterList where loadPhotoFromAlbums is null
        defaultShelterShouldNotBeFound("loadPhotoFromAlbums.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByLoadByTagsIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where loadByTags equals to DEFAULT_LOAD_BY_TAGS
        defaultShelterShouldBeFound("loadByTags.equals=" + DEFAULT_LOAD_BY_TAGS);

        // Get all the shelterList where loadByTags equals to UPDATED_LOAD_BY_TAGS
        defaultShelterShouldNotBeFound("loadByTags.equals=" + UPDATED_LOAD_BY_TAGS);
    }

    @Test
    @Transactional
    public void getAllSheltersByLoadByTagsIsInShouldWork() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where loadByTags in DEFAULT_LOAD_BY_TAGS or UPDATED_LOAD_BY_TAGS
        defaultShelterShouldBeFound("loadByTags.in=" + DEFAULT_LOAD_BY_TAGS + "," + UPDATED_LOAD_BY_TAGS);

        // Get all the shelterList where loadByTags equals to UPDATED_LOAD_BY_TAGS
        defaultShelterShouldNotBeFound("loadByTags.in=" + UPDATED_LOAD_BY_TAGS);
    }

    @Test
    @Transactional
    public void getAllSheltersByLoadByTagsIsNullOrNotNull() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        // Get all the shelterList where loadByTags is not null
        defaultShelterShouldBeFound("loadByTags.specified=true");

        // Get all the shelterList where loadByTags is null
        defaultShelterShouldNotBeFound("loadByTags.specified=false");
    }

    @Test
    @Transactional
    public void getAllSheltersByPetIsEqualToSomething() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);
        Pet pet = PetResourceIT.createEntity(em);
        em.persist(pet);
        em.flush();
        shelter.addPet(pet);
        shelterRepository.saveAndFlush(shelter);
        Long petId = pet.getId();

        // Get all the shelterList where pet equals to petId
        defaultShelterShouldBeFound("petId.equals=" + petId);

        // Get all the shelterList where pet equals to petId + 1
        defaultShelterShouldNotBeFound("petId.equals=" + (petId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultShelterShouldBeFound(String filter) throws Exception {
        restShelterMockMvc.perform(get("/api/shelters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shelter.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].link").value(hasItem(DEFAULT_LINK)))
            .andExpect(jsonPath("$.[*].groupId").value(hasItem(DEFAULT_GROUP_ID)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].regInfo").value(hasItem(DEFAULT_REG_INFO)))
            .andExpect(jsonPath("$.[*].needFeed").value(hasItem(DEFAULT_NEED_FEED.booleanValue())))
            .andExpect(jsonPath("$.[*].needHands").value(hasItem(DEFAULT_NEED_HANDS.booleanValue())))
            .andExpect(jsonPath("$.[*].needGrooming").value(hasItem(DEFAULT_NEED_GROOMING.booleanValue())))
            .andExpect(jsonPath("$.[*].needMedHelp").value(hasItem(DEFAULT_NEED_MED_HELP.booleanValue())))
            .andExpect(jsonPath("$.[*].needWalk").value(hasItem(DEFAULT_NEED_WALK.booleanValue())))
            .andExpect(jsonPath("$.[*].needOverexposure").value(hasItem(DEFAULT_NEED_OVEREXPOSURE.booleanValue())))
            .andExpect(jsonPath("$.[*].needGroupDesignHelp").value(hasItem(DEFAULT_NEED_GROUP_DESIGN_HELP.booleanValue())))
            .andExpect(jsonPath("$.[*].needRepair").value(hasItem(DEFAULT_NEED_REPAIR.booleanValue())))
            .andExpect(jsonPath("$.[*].needTransport").value(hasItem(DEFAULT_NEED_TRANSPORT.booleanValue())))
            .andExpect(jsonPath("$.[*].canPhotoSession").value(hasItem(DEFAULT_CAN_PHOTO_SESSION.booleanValue())))
            .andExpect(jsonPath("$.[*].canVisit").value(hasItem(DEFAULT_CAN_VISIT.booleanValue())))
            .andExpect(jsonPath("$.[*].loadAlbums").value(hasItem(DEFAULT_LOAD_ALBUMS)))
            .andExpect(jsonPath("$.[*].loadPhotoFromAlbums").value(hasItem(DEFAULT_LOAD_PHOTO_FROM_ALBUMS)))
            .andExpect(jsonPath("$.[*].loadByTags").value(hasItem(DEFAULT_LOAD_BY_TAGS.booleanValue())));

        // Check, that the count call also returns 1
        restShelterMockMvc.perform(get("/api/shelters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultShelterShouldNotBeFound(String filter) throws Exception {
        restShelterMockMvc.perform(get("/api/shelters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restShelterMockMvc.perform(get("/api/shelters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingShelter() throws Exception {
        // Get the shelter
        restShelterMockMvc.perform(get("/api/shelters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShelter() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        int databaseSizeBeforeUpdate = shelterRepository.findAll().size();

        // Update the shelter
        Shelter updatedShelter = shelterRepository.findById(shelter.getId()).get();
        // Disconnect from session so that the updates on updatedShelter are not directly saved in db
        em.detach(updatedShelter);
        updatedShelter
            .name(UPDATED_NAME)
            .link(UPDATED_LINK)
            .groupId(UPDATED_GROUP_ID)
            .city(UPDATED_CITY)
            .description(UPDATED_DESCRIPTION)
            .regInfo(UPDATED_REG_INFO)
            .needFeed(UPDATED_NEED_FEED)
            .needHands(UPDATED_NEED_HANDS)
            .needGrooming(UPDATED_NEED_GROOMING)
            .needMedHelp(UPDATED_NEED_MED_HELP)
            .needWalk(UPDATED_NEED_WALK)
            .needOverexposure(UPDATED_NEED_OVEREXPOSURE)
            .needGroupDesignHelp(UPDATED_NEED_GROUP_DESIGN_HELP)
            .needRepair(UPDATED_NEED_REPAIR)
            .needTransport(UPDATED_NEED_TRANSPORT)
            .canPhotoSession(UPDATED_CAN_PHOTO_SESSION)
            .canVisit(UPDATED_CAN_VISIT)
            .loadAlbums(UPDATED_LOAD_ALBUMS)
            .loadPhotoFromAlbums(UPDATED_LOAD_PHOTO_FROM_ALBUMS)
            .loadByTags(UPDATED_LOAD_BY_TAGS);
        ShelterDTO shelterDTO = shelterMapper.toDto(updatedShelter);

        restShelterMockMvc.perform(put("/api/shelters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shelterDTO)))
            .andExpect(status().isOk());

        // Validate the Shelter in the database
        List<Shelter> shelterList = shelterRepository.findAll();
        assertThat(shelterList).hasSize(databaseSizeBeforeUpdate);
        Shelter testShelter = shelterList.get(shelterList.size() - 1);
        assertThat(testShelter.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testShelter.getLink()).isEqualTo(UPDATED_LINK);
        assertThat(testShelter.getGroupId()).isEqualTo(UPDATED_GROUP_ID);
        assertThat(testShelter.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testShelter.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testShelter.getRegInfo()).isEqualTo(UPDATED_REG_INFO);
        assertThat(testShelter.isNeedFeed()).isEqualTo(UPDATED_NEED_FEED);
        assertThat(testShelter.isNeedHands()).isEqualTo(UPDATED_NEED_HANDS);
        assertThat(testShelter.isNeedGrooming()).isEqualTo(UPDATED_NEED_GROOMING);
        assertThat(testShelter.isNeedMedHelp()).isEqualTo(UPDATED_NEED_MED_HELP);
        assertThat(testShelter.isNeedWalk()).isEqualTo(UPDATED_NEED_WALK);
        assertThat(testShelter.isNeedOverexposure()).isEqualTo(UPDATED_NEED_OVEREXPOSURE);
        assertThat(testShelter.isNeedGroupDesignHelp()).isEqualTo(UPDATED_NEED_GROUP_DESIGN_HELP);
        assertThat(testShelter.isNeedRepair()).isEqualTo(UPDATED_NEED_REPAIR);
        assertThat(testShelter.isNeedTransport()).isEqualTo(UPDATED_NEED_TRANSPORT);
        assertThat(testShelter.isCanPhotoSession()).isEqualTo(UPDATED_CAN_PHOTO_SESSION);
        assertThat(testShelter.isCanVisit()).isEqualTo(UPDATED_CAN_VISIT);
        assertThat(testShelter.getLoadAlbums()).isEqualTo(UPDATED_LOAD_ALBUMS);
        assertThat(testShelter.getLoadPhotoFromAlbums()).isEqualTo(UPDATED_LOAD_PHOTO_FROM_ALBUMS);
        assertThat(testShelter.isLoadByTags()).isEqualTo(UPDATED_LOAD_BY_TAGS);

        // Validate the Shelter in Elasticsearch
        verify(mockShelterSearchRepository, times(1)).save(testShelter);
    }

    @Test
    @Transactional
    public void updateNonExistingShelter() throws Exception {
        int databaseSizeBeforeUpdate = shelterRepository.findAll().size();

        // Create the Shelter
        ShelterDTO shelterDTO = shelterMapper.toDto(shelter);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShelterMockMvc.perform(put("/api/shelters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shelterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Shelter in the database
        List<Shelter> shelterList = shelterRepository.findAll();
        assertThat(shelterList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Shelter in Elasticsearch
        verify(mockShelterSearchRepository, times(0)).save(shelter);
    }

    @Test
    @Transactional
    public void deleteShelter() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);

        int databaseSizeBeforeDelete = shelterRepository.findAll().size();

        // Delete the shelter
        restShelterMockMvc.perform(delete("/api/shelters/{id}", shelter.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Shelter> shelterList = shelterRepository.findAll();
        assertThat(shelterList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Shelter in Elasticsearch
        verify(mockShelterSearchRepository, times(1)).deleteById(shelter.getId());
    }

    @Test
    @Transactional
    public void searchShelter() throws Exception {
        // Initialize the database
        shelterRepository.saveAndFlush(shelter);
        when(mockShelterSearchRepository.search(queryStringQuery("id:" + shelter.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(shelter), PageRequest.of(0, 1), 1));
        // Search the shelter
        restShelterMockMvc.perform(get("/api/_search/shelters?query=id:" + shelter.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shelter.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].link").value(hasItem(DEFAULT_LINK)))
            .andExpect(jsonPath("$.[*].groupId").value(hasItem(DEFAULT_GROUP_ID)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].regInfo").value(hasItem(DEFAULT_REG_INFO)))
            .andExpect(jsonPath("$.[*].needFeed").value(hasItem(DEFAULT_NEED_FEED.booleanValue())))
            .andExpect(jsonPath("$.[*].needHands").value(hasItem(DEFAULT_NEED_HANDS.booleanValue())))
            .andExpect(jsonPath("$.[*].needGrooming").value(hasItem(DEFAULT_NEED_GROOMING.booleanValue())))
            .andExpect(jsonPath("$.[*].needMedHelp").value(hasItem(DEFAULT_NEED_MED_HELP.booleanValue())))
            .andExpect(jsonPath("$.[*].needWalk").value(hasItem(DEFAULT_NEED_WALK.booleanValue())))
            .andExpect(jsonPath("$.[*].needOverexposure").value(hasItem(DEFAULT_NEED_OVEREXPOSURE.booleanValue())))
            .andExpect(jsonPath("$.[*].needGroupDesignHelp").value(hasItem(DEFAULT_NEED_GROUP_DESIGN_HELP.booleanValue())))
            .andExpect(jsonPath("$.[*].needRepair").value(hasItem(DEFAULT_NEED_REPAIR.booleanValue())))
            .andExpect(jsonPath("$.[*].needTransport").value(hasItem(DEFAULT_NEED_TRANSPORT.booleanValue())))
            .andExpect(jsonPath("$.[*].canPhotoSession").value(hasItem(DEFAULT_CAN_PHOTO_SESSION.booleanValue())))
            .andExpect(jsonPath("$.[*].canVisit").value(hasItem(DEFAULT_CAN_VISIT.booleanValue())))
            .andExpect(jsonPath("$.[*].loadAlbums").value(hasItem(DEFAULT_LOAD_ALBUMS)))
            .andExpect(jsonPath("$.[*].loadPhotoFromAlbums").value(hasItem(DEFAULT_LOAD_PHOTO_FROM_ALBUMS)))
            .andExpect(jsonPath("$.[*].loadByTags").value(hasItem(DEFAULT_LOAD_BY_TAGS.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Shelter.class);
        Shelter shelter1 = new Shelter();
        shelter1.setId(1L);
        Shelter shelter2 = new Shelter();
        shelter2.setId(shelter1.getId());
        assertThat(shelter1).isEqualTo(shelter2);
        shelter2.setId(2L);
        assertThat(shelter1).isNotEqualTo(shelter2);
        shelter1.setId(null);
        assertThat(shelter1).isNotEqualTo(shelter2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShelterDTO.class);
        ShelterDTO shelterDTO1 = new ShelterDTO();
        shelterDTO1.setId(1L);
        ShelterDTO shelterDTO2 = new ShelterDTO();
        assertThat(shelterDTO1).isNotEqualTo(shelterDTO2);
        shelterDTO2.setId(shelterDTO1.getId());
        assertThat(shelterDTO1).isEqualTo(shelterDTO2);
        shelterDTO2.setId(2L);
        assertThat(shelterDTO1).isNotEqualTo(shelterDTO2);
        shelterDTO1.setId(null);
        assertThat(shelterDTO1).isNotEqualTo(shelterDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(shelterMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(shelterMapper.fromId(null)).isNull();
    }
}
