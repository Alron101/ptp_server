package ru.petthepet.server.web.rest;

import ru.petthepet.server.PtpApp;
import ru.petthepet.server.domain.Pet;
import ru.petthepet.server.domain.Shelter;
import ru.petthepet.server.repository.PetRepository;
import ru.petthepet.server.repository.search.PetSearchRepository;
import ru.petthepet.server.service.PetService;
import ru.petthepet.server.service.dto.PetDTO;
import ru.petthepet.server.service.mapper.PetMapper;
import ru.petthepet.server.web.rest.errors.ExceptionTranslator;
import ru.petthepet.server.service.dto.PetCriteria;
import ru.petthepet.server.service.PetQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static ru.petthepet.server.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PetResource} REST controller.
 */
@SpringBootTest(classes = PtpApp.class)
public class PetResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Boolean DEFAULT_SEX = false;
    private static final Boolean UPDATED_SEX = true;

    private static final Integer DEFAULT_AGE = 1;
    private static final Integer UPDATED_AGE = 2;
    private static final Integer SMALLER_AGE = 1 - 1;

    private static final String DEFAULT_PHOTO = "AAAAAAAAAA";
    private static final String UPDATED_PHOTO = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_BREED = "AAAAAAAAAA";
    private static final String UPDATED_BREED = "BBBBBBBBBB";

    private static final String DEFAULT_GENUS = "AAAAAAAAAA";
    private static final String UPDATED_GENUS = "BBBBBBBBBB";

    @Autowired
    private PetRepository petRepository;

    @Autowired
    private PetMapper petMapper;

    @Autowired
    private PetService petService;

    /**
     * This repository is mocked in the ru.petthepet.server.repository.search test package.
     *
     * @see ru.petthepet.server.repository.search.PetSearchRepositoryMockConfiguration
     */
    @Autowired
    private PetSearchRepository mockPetSearchRepository;

    @Autowired
    private PetQueryService petQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPetMockMvc;

    private Pet pet;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PetResource petResource = new PetResource(petService, petQueryService);
        this.restPetMockMvc = MockMvcBuilders.standaloneSetup(petResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pet createEntity(EntityManager em) {
        Pet pet = new Pet()
            .name(DEFAULT_NAME)
            .sex(DEFAULT_SEX)
            .age(DEFAULT_AGE)
            .photo(DEFAULT_PHOTO)
            .description(DEFAULT_DESCRIPTION)
            .breed(DEFAULT_BREED)
            .genus(DEFAULT_GENUS);
        return pet;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pet createUpdatedEntity(EntityManager em) {
        Pet pet = new Pet()
            .name(UPDATED_NAME)
            .sex(UPDATED_SEX)
            .age(UPDATED_AGE)
            .photo(UPDATED_PHOTO)
            .description(UPDATED_DESCRIPTION)
            .breed(UPDATED_BREED)
            .genus(UPDATED_GENUS);
        return pet;
    }

    @BeforeEach
    public void initTest() {
        pet = createEntity(em);
    }

    @Test
    @Transactional
    public void createPet() throws Exception {
        int databaseSizeBeforeCreate = petRepository.findAll().size();

        // Create the Pet
        PetDTO petDTO = petMapper.toDto(pet);
        restPetMockMvc.perform(post("/api/pets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(petDTO)))
            .andExpect(status().isCreated());

        // Validate the Pet in the database
        List<Pet> petList = petRepository.findAll();
        assertThat(petList).hasSize(databaseSizeBeforeCreate + 1);
        Pet testPet = petList.get(petList.size() - 1);
        assertThat(testPet.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPet.isSex()).isEqualTo(DEFAULT_SEX);
        assertThat(testPet.getAge()).isEqualTo(DEFAULT_AGE);
        assertThat(testPet.getPhoto()).isEqualTo(DEFAULT_PHOTO);
        assertThat(testPet.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPet.getBreed()).isEqualTo(DEFAULT_BREED);
        assertThat(testPet.getGenus()).isEqualTo(DEFAULT_GENUS);

        // Validate the Pet in Elasticsearch
        verify(mockPetSearchRepository, times(1)).save(testPet);
    }

    @Test
    @Transactional
    public void createPetWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = petRepository.findAll().size();

        // Create the Pet with an existing ID
        pet.setId(1L);
        PetDTO petDTO = petMapper.toDto(pet);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPetMockMvc.perform(post("/api/pets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(petDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Pet in the database
        List<Pet> petList = petRepository.findAll();
        assertThat(petList).hasSize(databaseSizeBeforeCreate);

        // Validate the Pet in Elasticsearch
        verify(mockPetSearchRepository, times(0)).save(pet);
    }


    @Test
    @Transactional
    public void getAllPets() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList
        restPetMockMvc.perform(get("/api/pets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pet.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].sex").value(hasItem(DEFAULT_SEX.booleanValue())))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE)))
            .andExpect(jsonPath("$.[*].photo").value(hasItem(DEFAULT_PHOTO.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].breed").value(hasItem(DEFAULT_BREED.toString())))
            .andExpect(jsonPath("$.[*].genus").value(hasItem(DEFAULT_GENUS.toString())));
    }
    
    @Test
    @Transactional
    public void getPet() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get the pet
        restPetMockMvc.perform(get("/api/pets/{id}", pet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pet.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.sex").value(DEFAULT_SEX.booleanValue()))
            .andExpect(jsonPath("$.age").value(DEFAULT_AGE))
            .andExpect(jsonPath("$.photo").value(DEFAULT_PHOTO.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.breed").value(DEFAULT_BREED.toString()))
            .andExpect(jsonPath("$.genus").value(DEFAULT_GENUS.toString()));
    }

    @Test
    @Transactional
    public void getAllPetsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where name equals to DEFAULT_NAME
        defaultPetShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the petList where name equals to UPDATED_NAME
        defaultPetShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllPetsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where name in DEFAULT_NAME or UPDATED_NAME
        defaultPetShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the petList where name equals to UPDATED_NAME
        defaultPetShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllPetsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where name is not null
        defaultPetShouldBeFound("name.specified=true");

        // Get all the petList where name is null
        defaultPetShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllPetsBySexIsEqualToSomething() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where sex equals to DEFAULT_SEX
        defaultPetShouldBeFound("sex.equals=" + DEFAULT_SEX);

        // Get all the petList where sex equals to UPDATED_SEX
        defaultPetShouldNotBeFound("sex.equals=" + UPDATED_SEX);
    }

    @Test
    @Transactional
    public void getAllPetsBySexIsInShouldWork() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where sex in DEFAULT_SEX or UPDATED_SEX
        defaultPetShouldBeFound("sex.in=" + DEFAULT_SEX + "," + UPDATED_SEX);

        // Get all the petList where sex equals to UPDATED_SEX
        defaultPetShouldNotBeFound("sex.in=" + UPDATED_SEX);
    }

    @Test
    @Transactional
    public void getAllPetsBySexIsNullOrNotNull() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where sex is not null
        defaultPetShouldBeFound("sex.specified=true");

        // Get all the petList where sex is null
        defaultPetShouldNotBeFound("sex.specified=false");
    }

    @Test
    @Transactional
    public void getAllPetsByAgeIsEqualToSomething() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where age equals to DEFAULT_AGE
        defaultPetShouldBeFound("age.equals=" + DEFAULT_AGE);

        // Get all the petList where age equals to UPDATED_AGE
        defaultPetShouldNotBeFound("age.equals=" + UPDATED_AGE);
    }

    @Test
    @Transactional
    public void getAllPetsByAgeIsInShouldWork() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where age in DEFAULT_AGE or UPDATED_AGE
        defaultPetShouldBeFound("age.in=" + DEFAULT_AGE + "," + UPDATED_AGE);

        // Get all the petList where age equals to UPDATED_AGE
        defaultPetShouldNotBeFound("age.in=" + UPDATED_AGE);
    }

    @Test
    @Transactional
    public void getAllPetsByAgeIsNullOrNotNull() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where age is not null
        defaultPetShouldBeFound("age.specified=true");

        // Get all the petList where age is null
        defaultPetShouldNotBeFound("age.specified=false");
    }

    @Test
    @Transactional
    public void getAllPetsByAgeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where age is greater than or equal to DEFAULT_AGE
        defaultPetShouldBeFound("age.greaterThanOrEqual=" + DEFAULT_AGE);

        // Get all the petList where age is greater than or equal to UPDATED_AGE
        defaultPetShouldNotBeFound("age.greaterThanOrEqual=" + UPDATED_AGE);
    }

    @Test
    @Transactional
    public void getAllPetsByAgeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where age is less than or equal to DEFAULT_AGE
        defaultPetShouldBeFound("age.lessThanOrEqual=" + DEFAULT_AGE);

        // Get all the petList where age is less than or equal to SMALLER_AGE
        defaultPetShouldNotBeFound("age.lessThanOrEqual=" + SMALLER_AGE);
    }

    @Test
    @Transactional
    public void getAllPetsByAgeIsLessThanSomething() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where age is less than DEFAULT_AGE
        defaultPetShouldNotBeFound("age.lessThan=" + DEFAULT_AGE);

        // Get all the petList where age is less than UPDATED_AGE
        defaultPetShouldBeFound("age.lessThan=" + UPDATED_AGE);
    }

    @Test
    @Transactional
    public void getAllPetsByAgeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where age is greater than DEFAULT_AGE
        defaultPetShouldNotBeFound("age.greaterThan=" + DEFAULT_AGE);

        // Get all the petList where age is greater than SMALLER_AGE
        defaultPetShouldBeFound("age.greaterThan=" + SMALLER_AGE);
    }


    @Test
    @Transactional
    public void getAllPetsByPhotoIsEqualToSomething() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where photo equals to DEFAULT_PHOTO
        defaultPetShouldBeFound("photo.equals=" + DEFAULT_PHOTO);

        // Get all the petList where photo equals to UPDATED_PHOTO
        defaultPetShouldNotBeFound("photo.equals=" + UPDATED_PHOTO);
    }

    @Test
    @Transactional
    public void getAllPetsByPhotoIsInShouldWork() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where photo in DEFAULT_PHOTO or UPDATED_PHOTO
        defaultPetShouldBeFound("photo.in=" + DEFAULT_PHOTO + "," + UPDATED_PHOTO);

        // Get all the petList where photo equals to UPDATED_PHOTO
        defaultPetShouldNotBeFound("photo.in=" + UPDATED_PHOTO);
    }

    @Test
    @Transactional
    public void getAllPetsByPhotoIsNullOrNotNull() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where photo is not null
        defaultPetShouldBeFound("photo.specified=true");

        // Get all the petList where photo is null
        defaultPetShouldNotBeFound("photo.specified=false");
    }

    @Test
    @Transactional
    public void getAllPetsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where description equals to DEFAULT_DESCRIPTION
        defaultPetShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the petList where description equals to UPDATED_DESCRIPTION
        defaultPetShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllPetsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultPetShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the petList where description equals to UPDATED_DESCRIPTION
        defaultPetShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllPetsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where description is not null
        defaultPetShouldBeFound("description.specified=true");

        // Get all the petList where description is null
        defaultPetShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllPetsByBreedIsEqualToSomething() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where breed equals to DEFAULT_BREED
        defaultPetShouldBeFound("breed.equals=" + DEFAULT_BREED);

        // Get all the petList where breed equals to UPDATED_BREED
        defaultPetShouldNotBeFound("breed.equals=" + UPDATED_BREED);
    }

    @Test
    @Transactional
    public void getAllPetsByBreedIsInShouldWork() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where breed in DEFAULT_BREED or UPDATED_BREED
        defaultPetShouldBeFound("breed.in=" + DEFAULT_BREED + "," + UPDATED_BREED);

        // Get all the petList where breed equals to UPDATED_BREED
        defaultPetShouldNotBeFound("breed.in=" + UPDATED_BREED);
    }

    @Test
    @Transactional
    public void getAllPetsByBreedIsNullOrNotNull() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where breed is not null
        defaultPetShouldBeFound("breed.specified=true");

        // Get all the petList where breed is null
        defaultPetShouldNotBeFound("breed.specified=false");
    }

    @Test
    @Transactional
    public void getAllPetsByGenusIsEqualToSomething() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where genus equals to DEFAULT_GENUS
        defaultPetShouldBeFound("genus.equals=" + DEFAULT_GENUS);

        // Get all the petList where genus equals to UPDATED_GENUS
        defaultPetShouldNotBeFound("genus.equals=" + UPDATED_GENUS);
    }

    @Test
    @Transactional
    public void getAllPetsByGenusIsInShouldWork() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where genus in DEFAULT_GENUS or UPDATED_GENUS
        defaultPetShouldBeFound("genus.in=" + DEFAULT_GENUS + "," + UPDATED_GENUS);

        // Get all the petList where genus equals to UPDATED_GENUS
        defaultPetShouldNotBeFound("genus.in=" + UPDATED_GENUS);
    }

    @Test
    @Transactional
    public void getAllPetsByGenusIsNullOrNotNull() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        // Get all the petList where genus is not null
        defaultPetShouldBeFound("genus.specified=true");

        // Get all the petList where genus is null
        defaultPetShouldNotBeFound("genus.specified=false");
    }

    @Test
    @Transactional
    public void getAllPetsByShelterIsEqualToSomething() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);
        Shelter shelter = ShelterResourceIT.createEntity(em);
        em.persist(shelter);
        em.flush();
        pet.setShelter(shelter);
        petRepository.saveAndFlush(pet);
        Long shelterId = shelter.getId();

        // Get all the petList where shelter equals to shelterId
        defaultPetShouldBeFound("shelterId.equals=" + shelterId);

        // Get all the petList where shelter equals to shelterId + 1
        defaultPetShouldNotBeFound("shelterId.equals=" + (shelterId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPetShouldBeFound(String filter) throws Exception {
        restPetMockMvc.perform(get("/api/pets?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pet.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].sex").value(hasItem(DEFAULT_SEX.booleanValue())))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE)))
            .andExpect(jsonPath("$.[*].photo").value(hasItem(DEFAULT_PHOTO)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].breed").value(hasItem(DEFAULT_BREED)))
            .andExpect(jsonPath("$.[*].genus").value(hasItem(DEFAULT_GENUS)));

        // Check, that the count call also returns 1
        restPetMockMvc.perform(get("/api/pets/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPetShouldNotBeFound(String filter) throws Exception {
        restPetMockMvc.perform(get("/api/pets?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPetMockMvc.perform(get("/api/pets/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPet() throws Exception {
        // Get the pet
        restPetMockMvc.perform(get("/api/pets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePet() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        int databaseSizeBeforeUpdate = petRepository.findAll().size();

        // Update the pet
        Pet updatedPet = petRepository.findById(pet.getId()).get();
        // Disconnect from session so that the updates on updatedPet are not directly saved in db
        em.detach(updatedPet);
        updatedPet
            .name(UPDATED_NAME)
            .sex(UPDATED_SEX)
            .age(UPDATED_AGE)
            .photo(UPDATED_PHOTO)
            .description(UPDATED_DESCRIPTION)
            .breed(UPDATED_BREED)
            .genus(UPDATED_GENUS);
        PetDTO petDTO = petMapper.toDto(updatedPet);

        restPetMockMvc.perform(put("/api/pets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(petDTO)))
            .andExpect(status().isOk());

        // Validate the Pet in the database
        List<Pet> petList = petRepository.findAll();
        assertThat(petList).hasSize(databaseSizeBeforeUpdate);
        Pet testPet = petList.get(petList.size() - 1);
        assertThat(testPet.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPet.isSex()).isEqualTo(UPDATED_SEX);
        assertThat(testPet.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testPet.getPhoto()).isEqualTo(UPDATED_PHOTO);
        assertThat(testPet.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPet.getBreed()).isEqualTo(UPDATED_BREED);
        assertThat(testPet.getGenus()).isEqualTo(UPDATED_GENUS);

        // Validate the Pet in Elasticsearch
        verify(mockPetSearchRepository, times(1)).save(testPet);
    }

    @Test
    @Transactional
    public void updateNonExistingPet() throws Exception {
        int databaseSizeBeforeUpdate = petRepository.findAll().size();

        // Create the Pet
        PetDTO petDTO = petMapper.toDto(pet);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPetMockMvc.perform(put("/api/pets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(petDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Pet in the database
        List<Pet> petList = petRepository.findAll();
        assertThat(petList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Pet in Elasticsearch
        verify(mockPetSearchRepository, times(0)).save(pet);
    }

    @Test
    @Transactional
    public void deletePet() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);

        int databaseSizeBeforeDelete = petRepository.findAll().size();

        // Delete the pet
        restPetMockMvc.perform(delete("/api/pets/{id}", pet.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Pet> petList = petRepository.findAll();
        assertThat(petList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Pet in Elasticsearch
        verify(mockPetSearchRepository, times(1)).deleteById(pet.getId());
    }

    @Test
    @Transactional
    public void searchPet() throws Exception {
        // Initialize the database
        petRepository.saveAndFlush(pet);
        when(mockPetSearchRepository.search(queryStringQuery("id:" + pet.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(pet), PageRequest.of(0, 1), 1));
        // Search the pet
        restPetMockMvc.perform(get("/api/_search/pets?query=id:" + pet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pet.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].sex").value(hasItem(DEFAULT_SEX.booleanValue())))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE)))
            .andExpect(jsonPath("$.[*].photo").value(hasItem(DEFAULT_PHOTO)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].breed").value(hasItem(DEFAULT_BREED)))
            .andExpect(jsonPath("$.[*].genus").value(hasItem(DEFAULT_GENUS)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Pet.class);
        Pet pet1 = new Pet();
        pet1.setId(1L);
        Pet pet2 = new Pet();
        pet2.setId(pet1.getId());
        assertThat(pet1).isEqualTo(pet2);
        pet2.setId(2L);
        assertThat(pet1).isNotEqualTo(pet2);
        pet1.setId(null);
        assertThat(pet1).isNotEqualTo(pet2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PetDTO.class);
        PetDTO petDTO1 = new PetDTO();
        petDTO1.setId(1L);
        PetDTO petDTO2 = new PetDTO();
        assertThat(petDTO1).isNotEqualTo(petDTO2);
        petDTO2.setId(petDTO1.getId());
        assertThat(petDTO1).isEqualTo(petDTO2);
        petDTO2.setId(2L);
        assertThat(petDTO1).isNotEqualTo(petDTO2);
        petDTO1.setId(null);
        assertThat(petDTO1).isNotEqualTo(petDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(petMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(petMapper.fromId(null)).isNull();
    }
}
