package ru.petthepet.server.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link ShelterSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class ShelterSearchRepositoryMockConfiguration {

    @MockBean
    private ShelterSearchRepository mockShelterSearchRepository;

}
