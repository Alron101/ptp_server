package ru.petthepet.server.repository;
import ru.petthepet.server.domain.Shelter;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Shelter entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShelterRepository extends JpaRepository<Shelter, Long>, JpaSpecificationExecutor<Shelter> {

}
