package ru.petthepet.server.repository.search;
import ru.petthepet.server.domain.Shelter;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Shelter} entity.
 */
public interface ShelterSearchRepository extends ElasticsearchRepository<Shelter, Long> {
}
