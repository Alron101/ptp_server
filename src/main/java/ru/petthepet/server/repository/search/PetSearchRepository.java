package ru.petthepet.server.repository.search;
import ru.petthepet.server.domain.Pet;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Pet} entity.
 */
public interface PetSearchRepository extends ElasticsearchRepository<Pet, Long> {
}
