package ru.petthepet.server.service.mapper;

import ru.petthepet.server.domain.*;
import ru.petthepet.server.service.dto.PetDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Pet} and its DTO {@link PetDTO}.
 */
@Mapper(componentModel = "spring", uses = {ShelterMapper.class})
public interface PetMapper extends EntityMapper<PetDTO, Pet> {

    @Mapping(source = "shelter.id", target = "shelterId")
    @Mapping(source = "shelter.name", target = "shelterName")
    @Mapping(source = "shelter.payLink", target = "payLink")
    @Mapping(source = "shelter.chatLink", target = "chatLink")
    PetDTO toDto(Pet pet);

    @Mapping(source = "shelterId", target = "shelter")
    Pet toEntity(PetDTO petDTO);

    default Pet fromId(Long id) {
        if (id == null) {
            return null;
        }
        Pet pet = new Pet();
        pet.setId(id);
        return pet;
    }
}
