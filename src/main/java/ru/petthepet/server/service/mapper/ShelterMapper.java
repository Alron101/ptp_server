package ru.petthepet.server.service.mapper;

import ru.petthepet.server.domain.*;
import ru.petthepet.server.service.dto.ShelterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Shelter} and its DTO {@link ShelterDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ShelterMapper extends EntityMapper<ShelterDTO, Shelter> {


    @Mapping(target = "pets", ignore = true)
    @Mapping(target = "removePet", ignore = true)
    Shelter toEntity(ShelterDTO shelterDTO);

    @Mapping(source = "payLink", target = "payLink")
    @Mapping(source = "chatLink", target = "chatLink")
    ShelterDTO toDto(Shelter entity);

    default Shelter fromId(Long id) {
        if (id == null) {
            return null;
        }
        Shelter shelter = new Shelter();
        shelter.setId(id);
        return shelter;
    }
}
