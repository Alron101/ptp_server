package ru.petthepet.server.service;

import ru.petthepet.server.domain.Pet;
import ru.petthepet.server.repository.PetRepository;
import ru.petthepet.server.repository.search.PetSearchRepository;
import ru.petthepet.server.service.dto.PetDTO;
import ru.petthepet.server.service.mapper.PetMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Pet}.
 */
@Service
@Transactional
public class PetService {

    private final Logger log = LoggerFactory.getLogger(PetService.class);

    private final PetRepository petRepository;

    private final PetMapper petMapper;

    private final PetSearchRepository petSearchRepository;

    public PetService(PetRepository petRepository, PetMapper petMapper, PetSearchRepository petSearchRepository) {
        this.petRepository = petRepository;
        this.petMapper = petMapper;
        this.petSearchRepository = petSearchRepository;
    }

    /**
     * Save a pet.
     *
     * @param petDTO the entity to save.
     * @return the persisted entity.
     */
    public PetDTO save(PetDTO petDTO) {
        log.debug("Request to save Pet : {}", petDTO);
        Pet pet = petMapper.toEntity(petDTO);
        pet = petRepository.save(pet);
        PetDTO result = petMapper.toDto(pet);
        petSearchRepository.save(pet);
        return result;
    }

    /**
     * Get all the pets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<PetDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Pets");
        return petRepository.findAll(pageable)
            .map(petMapper::toDto);
    }


    /**
     * Get one pet by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PetDTO> findOne(Long id) {
        log.debug("Request to get Pet : {}", id);
        return petRepository.findById(id)
            .map(petMapper::toDto);
    }

    /**
     * Delete the pet by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Pet : {}", id);
        petRepository.deleteById(id);
        petSearchRepository.deleteById(id);
    }

    /**
     * Search for the pet corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<PetDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Pets for query {}", query);
        return petSearchRepository.search(queryStringQuery(query), pageable)
            .map(petMapper::toDto);
    }

    public void deleteAll() {
        petRepository.deleteAll();
    }

}
