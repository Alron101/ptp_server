package ru.petthepet.server.service;

import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.photos.Photo;
import com.vk.api.sdk.objects.photos.PhotoAlbumFull;
import com.vk.api.sdk.objects.photos.PhotoSizes;
import com.vk.api.sdk.objects.photos.PhotoSizesType;
import com.vk.api.sdk.objects.photos.responses.GetAlbumsResponse;
import com.vk.api.sdk.objects.wall.WallpostFull;
import com.vk.api.sdk.objects.wall.responses.GetResponse;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.petthepet.server.config.vk.VkConfig;
import ru.petthepet.server.service.dto.PetDTO;
import ru.petthepet.server.service.dto.ShelterDTO;
import ru.petthepet.server.web.rest.errors.VkApiCallException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class VkApiService {

    private ShelterService shelterService;
    private PetService petService;
    private VkConfig vkConfig;

    private static final String PTP_CAT = "#ptp_cat";
    private static final String PTP_DOG = "#ptp_dog";
    private static final String PTP_CATS = "#ptp_cats";
    private static final String PTP_DOGS = "#ptp_dogs";
    private static final String DOG = "dog";
    private static final String CAT = "cat";


    public void loadDataFromShelterGroups() throws VkApiCallException {
        petService.deleteAll();
        List<ShelterDTO> shelters = shelterService.findAll();
        for (ShelterDTO shelter : shelters) {
            if (shelter.isLoadByTags()) {
                loadAlbums(shelter);
            }
            String[] loadManyFromOne = shelter.getLoadAlbums().split(";");
            if (loadManyFromOne.length > 0 && !loadManyFromOne[0].isEmpty()) {
                for(String album : loadManyFromOne) {
                    loadDataFromAlbum(shelter, album);
                }
            }
            String[] loadOneByOne = shelter.getLoadPhotoFromAlbums().split(";");
            if (loadOneByOne.length > 0 && !loadOneByOne[0].isEmpty()) {
                for(String album : loadOneByOne) {
                    loadFirstPhoto(shelter, album);
                }
            }
//            loadWallMessages(shelter.getGroupId());
        }
    }

    private void loadFirstPhoto(ShelterDTO shelter, String albumId) {
        try {
            PhotoAlbumFull album = vkConfig.getVk().photos().getAlbums(vkConfig.getServiceActor())
                .ownerId(shelter.getGroupId())
                .albumIds(Integer.valueOf(albumId))
                .count(1)
                .execute().getItems().get(0);
            String petName = album.getTitle();
            Photo photo = vkConfig.getVk().photos().get(vkConfig.getServiceActor())
                .ownerId(shelter.getGroupId())
                .albumId(albumId)
                .count(1)
                .execute().getItems().get(0);
            PetDTO pet = new PetDTO();
            List<PhotoSizes> result = photo.getSizes().stream().filter(p -> p.getType().equals(PhotoSizesType.R))
                .collect(Collectors.toList());
            if (result.isEmpty()) {
                pet.setPhoto(photo.getSizes().get(0).getUrl().toString());
            } else {
                pet.setPhoto(result.get(0).getUrl().toString());
            }
            pet.setName(petName);
            pet.setShelterId(shelter.getId());
            pet.setDescription(album.getDescription());
            pet.setGenus(DOG);
            petService.save(pet);
        } catch (ApiException | ClientException e) {
            e.printStackTrace();
        }
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void loadDataFromAlbum(ShelterDTO shelter, String albumId) {
        try {
            List<Photo> photoList = vkConfig.getVk().photos().get(vkConfig.getServiceActor())
                .ownerId(shelter.getGroupId())
                .albumId(albumId)
                .execute().getItems();
            for (Photo photo : photoList) {
                PetDTO pet = new PetDTO();
                List<PhotoSizes> result = photo.getSizes().stream().filter(p -> p.getType().equals(PhotoSizesType.R))
                    .collect(Collectors.toList());
                if (result.isEmpty()) {
                    pet.setPhoto(photo.getSizes().get(0).getUrl().toString());
                } else {
                    pet.setPhoto(result.get(0).getUrl().toString());
                }
                String text = photo.getText();
                String petName = "";
                if (!text.isEmpty()) {
                    String[] descr = text.split("\n");
                    if (descr.length > 0 && !descr[0].isEmpty()) {
                        petName = descr[0];
                    }
                }
                pet.setName(petName);
                pet.setShelterId(shelter.getId());
                pet.setDescription(photo.getText());
                pet.setGenus(CAT);
                petService.save(pet);
            }
        } catch (ApiException | ClientException e) {
            e.printStackTrace();
        }
    }

    private void loadAlbums(ShelterDTO shelter) throws VkApiCallException {
        try {
            Integer groupId = shelter.getGroupId();
            GetAlbumsResponse albums = vkConfig.getVk().photos().getAlbums(vkConfig.getServiceActor())
                .ownerId(groupId).execute();
            for (PhotoAlbumFull album : albums.getItems()) {
                if (album.getDescription().contains(PTP_CAT)) {
//                    getFirstPhoto(shelter.getGroupId(), album.getId());
                } else if (album.getDescription().contains(PTP_DOG)) {
//                    getFirstPhoto(shelter.getGroupId(), album.getId());
                } else if (album.getDescription().contains(PTP_CATS)) {
//                    loadDataFromAlbum(shelter);
                } else if (album.getDescription().contains(PTP_DOGS)) {

                }
            }
        } catch (ApiException | ClientException e) {
            e.printStackTrace();
            throw new VkApiCallException(e);
        }
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new VkApiCallException(e);
        }
    }

    private void loadWallMessages(Integer groupId) throws VkApiCallException {
        try {
            GetResponse posts = vkConfig.getVk().wall().get(vkConfig.getServiceActor())
                .ownerId(groupId).execute();
            for (WallpostFull post : posts.getItems()) {
                if (post.getText().contains(PTP_CAT)) {

                } else if (post.getText().contains(PTP_DOG)) {

                } else {
                }
            }
        } catch (ApiException | ClientException e) {
            e.printStackTrace();
            throw new VkApiCallException(e);
        }
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new VkApiCallException(e);
        }
    }

}
