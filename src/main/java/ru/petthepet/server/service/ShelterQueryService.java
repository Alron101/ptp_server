package ru.petthepet.server.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ru.petthepet.server.domain.Shelter;
import ru.petthepet.server.domain.*; // for static metamodels
import ru.petthepet.server.repository.ShelterRepository;
import ru.petthepet.server.repository.search.ShelterSearchRepository;
import ru.petthepet.server.service.dto.ShelterCriteria;
import ru.petthepet.server.service.dto.ShelterDTO;
import ru.petthepet.server.service.mapper.ShelterMapper;

/**
 * Service for executing complex queries for {@link Shelter} entities in the database.
 * The main input is a {@link ShelterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ShelterDTO} or a {@link Page} of {@link ShelterDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ShelterQueryService extends QueryService<Shelter> {

    private final Logger log = LoggerFactory.getLogger(ShelterQueryService.class);

    private final ShelterRepository shelterRepository;

    private final ShelterMapper shelterMapper;

    private final ShelterSearchRepository shelterSearchRepository;

    public ShelterQueryService(ShelterRepository shelterRepository, ShelterMapper shelterMapper, ShelterSearchRepository shelterSearchRepository) {
        this.shelterRepository = shelterRepository;
        this.shelterMapper = shelterMapper;
        this.shelterSearchRepository = shelterSearchRepository;
    }

    /**
     * Return a {@link List} of {@link ShelterDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ShelterDTO> findByCriteria(ShelterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Shelter> specification = createSpecification(criteria);
        return shelterMapper.toDto(shelterRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ShelterDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ShelterDTO> findByCriteria(ShelterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Shelter> specification = createSpecification(criteria);
        return shelterRepository.findAll(specification, page)
            .map(shelterMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ShelterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Shelter> specification = createSpecification(criteria);
        return shelterRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    protected Specification<Shelter> createSpecification(ShelterCriteria criteria) {
        Specification<Shelter> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Shelter_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Shelter_.name));
            }
            if (criteria.getLink() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLink(), Shelter_.link));
            }
            if (criteria.getGroupId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGroupId(), Shelter_.groupId));
            }
            if (criteria.getCity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCity(), Shelter_.city));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Shelter_.description));
            }
            if (criteria.getRegInfo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegInfo(), Shelter_.regInfo));
            }
            if (criteria.getNeedFeed() != null) {
                specification = specification.and(buildSpecification(criteria.getNeedFeed(), Shelter_.needFeed));
            }
            if (criteria.getNeedHands() != null) {
                specification = specification.and(buildSpecification(criteria.getNeedHands(), Shelter_.needHands));
            }
            if (criteria.getNeedGrooming() != null) {
                specification = specification.and(buildSpecification(criteria.getNeedGrooming(), Shelter_.needGrooming));
            }
            if (criteria.getNeedMedHelp() != null) {
                specification = specification.and(buildSpecification(criteria.getNeedMedHelp(), Shelter_.needMedHelp));
            }
            if (criteria.getNeedWalk() != null) {
                specification = specification.and(buildSpecification(criteria.getNeedWalk(), Shelter_.needWalk));
            }
            if (criteria.getNeedOverexposure() != null) {
                specification = specification.and(buildSpecification(criteria.getNeedOverexposure(), Shelter_.needOverexposure));
            }
            if (criteria.getNeedGroupDesignHelp() != null) {
                specification = specification.and(buildSpecification(criteria.getNeedGroupDesignHelp(), Shelter_.needGroupDesignHelp));
            }
            if (criteria.getNeedRepair() != null) {
                specification = specification.and(buildSpecification(criteria.getNeedRepair(), Shelter_.needRepair));
            }
            if (criteria.getNeedTransport() != null) {
                specification = specification.and(buildSpecification(criteria.getNeedTransport(), Shelter_.needTransport));
            }
            if (criteria.getCanPhotoSession() != null) {
                specification = specification.and(buildSpecification(criteria.getCanPhotoSession(), Shelter_.canPhotoSession));
            }
            if (criteria.getCanVisit() != null) {
                specification = specification.and(buildSpecification(criteria.getCanVisit(), Shelter_.canVisit));
            }
            if (criteria.getLoadAlbums() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLoadAlbums(), Shelter_.loadAlbums));
            }
            if (criteria.getLoadPhotoFromAlbums() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLoadPhotoFromAlbums(), Shelter_.loadPhotoFromAlbums));
            }
            if (criteria.getLoadByTags() != null) {
                specification = specification.and(buildSpecification(criteria.getLoadByTags(), Shelter_.loadByTags));
            }
            if (criteria.getPetId() != null) {
                specification = specification.and(buildSpecification(criteria.getPetId(),
                    root -> root.join(Shelter_.pets, JoinType.LEFT).get(Pet_.id)));
            }
        }
        return specification;
    }
}
