package ru.petthepet.server.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.petthepet.server.domain.Shelter;
import ru.petthepet.server.repository.ShelterRepository;
import ru.petthepet.server.repository.search.ShelterSearchRepository;
import ru.petthepet.server.service.dto.ShelterDTO;
import ru.petthepet.server.service.mapper.ShelterMapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Shelter}.
 */
@Service
@Transactional
public class ShelterService {

    private final Logger log = LoggerFactory.getLogger(ShelterService.class);

    private final ShelterRepository shelterRepository;

    private final ShelterMapper shelterMapper;

    private final ShelterSearchRepository shelterSearchRepository;

    public ShelterService(ShelterRepository shelterRepository, ShelterMapper shelterMapper, ShelterSearchRepository shelterSearchRepository) {
        this.shelterRepository = shelterRepository;
        this.shelterMapper = shelterMapper;
        this.shelterSearchRepository = shelterSearchRepository;
    }

    /**
     * Save a shelter.
     *
     * @param shelterDTO the entity to save.
     * @return the persisted entity.
     */
    public ShelterDTO save(ShelterDTO shelterDTO) {
        log.debug("Request to save Shelter : {}", shelterDTO);
        Shelter shelter = shelterMapper.toEntity(shelterDTO);
        shelter = shelterRepository.save(shelter);
        ShelterDTO result = shelterMapper.toDto(shelter);
        shelterSearchRepository.save(shelter);
        return result;
    }

    /**
     * Get all the shelters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ShelterDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Shelters");
        return shelterRepository.findAll(pageable)
            .map(shelterMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<ShelterDTO> findAll() {
        log.debug("Request to get all Shelters");
        return shelterRepository.findAll().stream()
            .map(shelterMapper::toDto).collect(Collectors.toList());
    }


    /**
     * Get one shelter by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ShelterDTO> findOne(Long id) {
        log.debug("Request to get Shelter : {}", id);
        return shelterRepository.findById(id)
            .map(shelterMapper::toDto);
    }

    /**
     * Delete the shelter by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Shelter : {}", id);
        shelterRepository.deleteById(id);
        shelterSearchRepository.deleteById(id);
    }

    /**
     * Search for the shelter corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ShelterDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Shelters for query {}", query);
        return shelterSearchRepository.search(queryStringQuery(query), pageable)
            .map(shelterMapper::toDto);
    }
}
