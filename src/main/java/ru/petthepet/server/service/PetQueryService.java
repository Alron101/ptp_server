package ru.petthepet.server.service;

import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.petthepet.server.domain.Pet;
import ru.petthepet.server.domain.Pet_;
import ru.petthepet.server.domain.Shelter_;
import ru.petthepet.server.repository.PetRepository;
import ru.petthepet.server.repository.search.PetSearchRepository;
import ru.petthepet.server.service.dto.PetCriteria;
import ru.petthepet.server.service.dto.PetDTO;
import ru.petthepet.server.service.mapper.PetMapper;

import javax.persistence.criteria.JoinType;
import java.util.List;

/**
 * Service for executing complex queries for {@link Pet} entities in the database.
 * The main input is a {@link PetCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PetDTO} or a {@link Page} of {@link PetDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PetQueryService extends QueryService<Pet> {

    private final Logger log = LoggerFactory.getLogger(PetQueryService.class);

    private final PetRepository petRepository;

    private final PetMapper petMapper;

    private final PetSearchRepository petSearchRepository;

    public PetQueryService(PetRepository petRepository, PetMapper petMapper, PetSearchRepository petSearchRepository) {
        this.petRepository = petRepository;
        this.petMapper = petMapper;
        this.petSearchRepository = petSearchRepository;
    }

    /**
     * Return a {@link List} of {@link PetDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PetDTO> findByCriteria(PetCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Pet> specification = createSpecification(criteria);
        return petMapper.toDto(petRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PetDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PetDTO> findByCriteria(PetCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Pet> specification = createSpecification(criteria);
        return petRepository.findAll(specification, page)
            .map(petMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PetCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Pet> specification = createSpecification(criteria);
        return petRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Pet> createSpecification(PetCriteria criteria) {
        Specification<Pet> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Pet_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Pet_.name));
            }
            if (criteria.getSex() != null) {
                specification = specification.and(buildSpecification(criteria.getSex(), Pet_.sex));
            }
            if (criteria.getAge() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAge(), Pet_.age));
            }
            if (criteria.getPhoto() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhoto(), Pet_.photo));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Pet_.description));
            }
            if (criteria.getBreed() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBreed(), Pet_.breed));
            }
            if (criteria.getGenus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGenus(), Pet_.genus));
            }
            if (criteria.getShelterId() != null) {
                specification = specification.and(buildSpecification(criteria.getShelterId(),
                    root -> root.join(Pet_.shelter, JoinType.LEFT).get(Shelter_.id)));
            }
            if (criteria.getCity() != null) {
                specification = specification.and(
                    buildSpecification(criteria.getCity(),
                        root -> root.join(Pet_.shelter, JoinType.LEFT).get(Shelter_.city))
                );
            }
        }
        return specification;
    }
}
