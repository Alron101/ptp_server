package ru.petthepet.server.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link ru.petthepet.server.domain.Pet} entity.
 */
public class PetDTO implements Serializable {

    private Long id;

    private String name;

    private Boolean sex;

    private Integer age;

    private String photo;

    @Size(max = 4096)
    private String description;

    private String breed;

    private String genus;


    private Long shelterId;

    private String shelterName;

    private String payLink;

    private String chatLink;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isSex() {
        return sex == null ? false : sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age == null ? 0 : age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBreed() {
        return breed == null ? "Дворняжка" : breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getGenus() {
        return genus == null ? "dog" : genus;
    }

    public void setGenus(String genus) {
        this.genus = genus;
    }

    public Long getShelterId() {
        return shelterId;
    }

    public void setShelterId(Long shelterId) {
        this.shelterId = shelterId;
    }

    public String getShelterName() {
        return shelterName;
    }

    public void setShelterName(String shelterName) {
        this.shelterName = shelterName;
    }

    public String getPayLink() {
        return payLink;
    }

    public void setPayLink(String payLink) {
        this.payLink = payLink;
    }

    public String getChatLink() {
        return chatLink;
    }

    public void setChatLink(String chatLink) {
        this.chatLink = chatLink;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PetDTO petDTO = (PetDTO) o;
        if (petDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), petDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PetDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", sex='" + isSex() + "'" +
            ", age=" + getAge() +
            ", photo='" + getPhoto() + "'" +
            ", description='" + getDescription() + "'" +
            ", breed='" + getBreed() + "'" +
            ", genus='" + getGenus() + "'" +
            ", shelter=" + getShelterId() +
            ", shelter='" + getShelterName() + "'" +
            "}";
    }
}
