package ru.petthepet.server.service.dto;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link ru.petthepet.server.domain.Pet} entity. This class is used
 * in {@link ru.petthepet.server.web.rest.PetResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /pets?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PetCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private BooleanFilter sex;

    private IntegerFilter age;

    private StringFilter photo;

    private StringFilter description;

    private StringFilter breed;

    private StringFilter genus;

    private LongFilter shelterId;

    private StringFilter city;

    public PetCriteria() {
    }

    public PetCriteria(PetCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.sex = other.sex == null ? null : other.sex.copy();
        this.age = other.age == null ? null : other.age.copy();
        this.photo = other.photo == null ? null : other.photo.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.breed = other.breed == null ? null : other.breed.copy();
        this.genus = other.genus == null ? null : other.genus.copy();
        this.shelterId = other.shelterId == null ? null : other.shelterId.copy();
        this.city = other.city == null ? null : other.city.copy();
    }

    @Override
    public PetCriteria copy() {
        return new PetCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public BooleanFilter getSex() {
        return sex;
    }

    public void setSex(BooleanFilter sex) {
        this.sex = sex;
    }

    public IntegerFilter getAge() {
        return age;
    }

    public void setAge(IntegerFilter age) {
        this.age = age;
    }

    public StringFilter getPhoto() {
        return photo;
    }

    public void setPhoto(StringFilter photo) {
        this.photo = photo;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getBreed() {
        return breed;
    }

    public void setBreed(StringFilter breed) {
        this.breed = breed;
    }

    public StringFilter getGenus() {
        return genus;
    }

    public void setGenus(StringFilter genus) {
        this.genus = genus;
    }

    public LongFilter getShelterId() {
        return shelterId;
    }

    public void setShelterId(LongFilter shelterId) {
        this.shelterId = shelterId;
    }

    public StringFilter getCity() {
        return city;
    }

    public void setCity(StringFilter city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PetCriteria that = (PetCriteria) o;
        return
            Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(sex, that.sex) &&
                Objects.equals(age, that.age) &&
                Objects.equals(photo, that.photo) &&
                Objects.equals(description, that.description) &&
                Objects.equals(breed, that.breed) &&
                Objects.equals(genus, that.genus) &&
                Objects.equals(shelterId, that.shelterId) &&
                Objects.equals(city, that.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            name,
            sex,
            age,
            photo,
            description,
            breed,
            genus,
            shelterId
        );
    }

    @Override
    public String toString() {
        return "PetCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (sex != null ? "sex=" + sex + ", " : "") +
            (age != null ? "age=" + age + ", " : "") +
            (photo != null ? "photo=" + photo + ", " : "") +
            (description != null ? "description=" + description + ", " : "") +
            (breed != null ? "breed=" + breed + ", " : "") +
            (genus != null ? "genus=" + genus + ", " : "") +
            (shelterId != null ? "shelterId=" + shelterId + ", " : "") +
            (city != null ? "city=" + city + ", " : "") +
            "}";
    }

}
