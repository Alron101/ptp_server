package ru.petthepet.server.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link ru.petthepet.server.domain.Shelter} entity. This class is used
 * in {@link ru.petthepet.server.web.rest.ShelterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /shelters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ShelterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter link;

    private IntegerFilter groupId;

    private StringFilter city;

    private StringFilter description;

    private StringFilter regInfo;

    private BooleanFilter needFeed;

    private BooleanFilter needHands;

    private BooleanFilter needGrooming;

    private BooleanFilter needMedHelp;

    private BooleanFilter needWalk;

    private BooleanFilter needOverexposure;

    private BooleanFilter needGroupDesignHelp;

    private BooleanFilter needRepair;

    private BooleanFilter needTransport;

    private BooleanFilter canPhotoSession;

    private BooleanFilter canVisit;

    private StringFilter loadAlbums;

    private StringFilter loadPhotoFromAlbums;

    private BooleanFilter loadByTags;

    private LongFilter petId;

    public ShelterCriteria(){
    }

    public ShelterCriteria(ShelterCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.link = other.link == null ? null : other.link.copy();
        this.groupId = other.groupId == null ? null : other.groupId.copy();
        this.city = other.city == null ? null : other.city.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.regInfo = other.regInfo == null ? null : other.regInfo.copy();
        this.needFeed = other.needFeed == null ? null : other.needFeed.copy();
        this.needHands = other.needHands == null ? null : other.needHands.copy();
        this.needGrooming = other.needGrooming == null ? null : other.needGrooming.copy();
        this.needMedHelp = other.needMedHelp == null ? null : other.needMedHelp.copy();
        this.needWalk = other.needWalk == null ? null : other.needWalk.copy();
        this.needOverexposure = other.needOverexposure == null ? null : other.needOverexposure.copy();
        this.needGroupDesignHelp = other.needGroupDesignHelp == null ? null : other.needGroupDesignHelp.copy();
        this.needRepair = other.needRepair == null ? null : other.needRepair.copy();
        this.needTransport = other.needTransport == null ? null : other.needTransport.copy();
        this.canPhotoSession = other.canPhotoSession == null ? null : other.canPhotoSession.copy();
        this.canVisit = other.canVisit == null ? null : other.canVisit.copy();
        this.loadAlbums = other.loadAlbums == null ? null : other.loadAlbums.copy();
        this.loadPhotoFromAlbums = other.loadPhotoFromAlbums == null ? null : other.loadPhotoFromAlbums.copy();
        this.loadByTags = other.loadByTags == null ? null : other.loadByTags.copy();
        this.petId = other.petId == null ? null : other.petId.copy();
    }

    @Override
    public ShelterCriteria copy() {
        return new ShelterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getLink() {
        return link;
    }

    public void setLink(StringFilter link) {
        this.link = link;
    }

    public IntegerFilter getGroupId() {
        return groupId;
    }

    public void setGroupId(IntegerFilter groupId) {
        this.groupId = groupId;
    }

    public StringFilter getCity() {
        return city;
    }

    public void setCity(StringFilter city) {
        this.city = city;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getRegInfo() {
        return regInfo;
    }

    public void setRegInfo(StringFilter regInfo) {
        this.regInfo = regInfo;
    }

    public BooleanFilter getNeedFeed() {
        return needFeed;
    }

    public void setNeedFeed(BooleanFilter needFeed) {
        this.needFeed = needFeed;
    }

    public BooleanFilter getNeedHands() {
        return needHands;
    }

    public void setNeedHands(BooleanFilter needHands) {
        this.needHands = needHands;
    }

    public BooleanFilter getNeedGrooming() {
        return needGrooming;
    }

    public void setNeedGrooming(BooleanFilter needGrooming) {
        this.needGrooming = needGrooming;
    }

    public BooleanFilter getNeedMedHelp() {
        return needMedHelp;
    }

    public void setNeedMedHelp(BooleanFilter needMedHelp) {
        this.needMedHelp = needMedHelp;
    }

    public BooleanFilter getNeedWalk() {
        return needWalk;
    }

    public void setNeedWalk(BooleanFilter needWalk) {
        this.needWalk = needWalk;
    }

    public BooleanFilter getNeedOverexposure() {
        return needOverexposure;
    }

    public void setNeedOverexposure(BooleanFilter needOverexposure) {
        this.needOverexposure = needOverexposure;
    }

    public BooleanFilter getNeedGroupDesignHelp() {
        return needGroupDesignHelp;
    }

    public void setNeedGroupDesignHelp(BooleanFilter needGroupDesignHelp) {
        this.needGroupDesignHelp = needGroupDesignHelp;
    }

    public BooleanFilter getNeedRepair() {
        return needRepair;
    }

    public void setNeedRepair(BooleanFilter needRepair) {
        this.needRepair = needRepair;
    }

    public BooleanFilter getNeedTransport() {
        return needTransport;
    }

    public void setNeedTransport(BooleanFilter needTransport) {
        this.needTransport = needTransport;
    }

    public BooleanFilter getCanPhotoSession() {
        return canPhotoSession;
    }

    public void setCanPhotoSession(BooleanFilter canPhotoSession) {
        this.canPhotoSession = canPhotoSession;
    }

    public BooleanFilter getCanVisit() {
        return canVisit;
    }

    public void setCanVisit(BooleanFilter canVisit) {
        this.canVisit = canVisit;
    }

    public StringFilter getLoadAlbums() {
        return loadAlbums;
    }

    public void setLoadAlbums(StringFilter loadAlbums) {
        this.loadAlbums = loadAlbums;
    }

    public StringFilter getLoadPhotoFromAlbums() {
        return loadPhotoFromAlbums;
    }

    public void setLoadPhotoFromAlbums(StringFilter loadPhotoFromAlbums) {
        this.loadPhotoFromAlbums = loadPhotoFromAlbums;
    }

    public BooleanFilter getLoadByTags() {
        return loadByTags;
    }

    public void setLoadByTags(BooleanFilter loadByTags) {
        this.loadByTags = loadByTags;
    }

    public LongFilter getPetId() {
        return petId;
    }

    public void setPetId(LongFilter petId) {
        this.petId = petId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ShelterCriteria that = (ShelterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(link, that.link) &&
            Objects.equals(groupId, that.groupId) &&
            Objects.equals(city, that.city) &&
            Objects.equals(description, that.description) &&
            Objects.equals(regInfo, that.regInfo) &&
            Objects.equals(needFeed, that.needFeed) &&
            Objects.equals(needHands, that.needHands) &&
            Objects.equals(needGrooming, that.needGrooming) &&
            Objects.equals(needMedHelp, that.needMedHelp) &&
            Objects.equals(needWalk, that.needWalk) &&
            Objects.equals(needOverexposure, that.needOverexposure) &&
            Objects.equals(needGroupDesignHelp, that.needGroupDesignHelp) &&
            Objects.equals(needRepair, that.needRepair) &&
            Objects.equals(needTransport, that.needTransport) &&
            Objects.equals(canPhotoSession, that.canPhotoSession) &&
            Objects.equals(canVisit, that.canVisit) &&
            Objects.equals(loadAlbums, that.loadAlbums) &&
            Objects.equals(loadPhotoFromAlbums, that.loadPhotoFromAlbums) &&
            Objects.equals(loadByTags, that.loadByTags) &&
            Objects.equals(petId, that.petId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        link,
        groupId,
        city,
        description,
        regInfo,
        needFeed,
        needHands,
        needGrooming,
        needMedHelp,
        needWalk,
        needOverexposure,
        needGroupDesignHelp,
        needRepair,
        needTransport,
        canPhotoSession,
        canVisit,
        loadAlbums,
        loadPhotoFromAlbums,
        loadByTags,
        petId
        );
    }

    @Override
    public String toString() {
        return "ShelterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (link != null ? "link=" + link + ", " : "") +
                (groupId != null ? "groupId=" + groupId + ", " : "") +
                (city != null ? "city=" + city + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (regInfo != null ? "regInfo=" + regInfo + ", " : "") +
                (needFeed != null ? "needFeed=" + needFeed + ", " : "") +
                (needHands != null ? "needHands=" + needHands + ", " : "") +
                (needGrooming != null ? "needGrooming=" + needGrooming + ", " : "") +
                (needMedHelp != null ? "needMedHelp=" + needMedHelp + ", " : "") +
                (needWalk != null ? "needWalk=" + needWalk + ", " : "") +
                (needOverexposure != null ? "needOverexposure=" + needOverexposure + ", " : "") +
                (needGroupDesignHelp != null ? "needGroupDesignHelp=" + needGroupDesignHelp + ", " : "") +
                (needRepair != null ? "needRepair=" + needRepair + ", " : "") +
                (needTransport != null ? "needTransport=" + needTransport + ", " : "") +
                (canPhotoSession != null ? "canPhotoSession=" + canPhotoSession + ", " : "") +
                (canVisit != null ? "canVisit=" + canVisit + ", " : "") +
                (loadAlbums != null ? "loadAlbums=" + loadAlbums + ", " : "") +
                (loadPhotoFromAlbums != null ? "loadPhotoFromAlbums=" + loadPhotoFromAlbums + ", " : "") +
                (loadByTags != null ? "loadByTags=" + loadByTags + ", " : "") +
                (petId != null ? "petId=" + petId + ", " : "") +
            "}";
    }

}
