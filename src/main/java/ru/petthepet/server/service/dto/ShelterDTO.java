package ru.petthepet.server.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link ru.petthepet.server.domain.Shelter} entity.
 */
public class ShelterDTO implements Serializable {

    private Long id;

    private String name;

    private String link;

    private String logo;

    private Integer groupId;

    private String city;

    @Size(max = 4096)
    private String description;

    @Size(max = 4096)
    private String regInfo;

    private Boolean needFeed;

    private Boolean needHands;

    private Boolean needGrooming;

    private Boolean needMedHelp;

    private Boolean needWalk;

    private Boolean needOverexposure;

    private Boolean needGroupDesignHelp;

    private Boolean needRepair;

    private Boolean needTransport;

    private Boolean canPhotoSession;

    private Boolean canVisit;

    @Size(max = 4096)
    private String loadAlbums;

    @Size(max = 4096)
    private String loadPhotoFromAlbums;

    private Boolean loadByTags;

    private String payLink;

    private String chatLink;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRegInfo() {
        return regInfo;
    }

    public void setRegInfo(String regInfo) {
        this.regInfo = regInfo;
    }

    public Boolean isNeedFeed() {
        return needFeed;
    }

    public void setNeedFeed(Boolean needFeed) {
        this.needFeed = needFeed;
    }

    public Boolean isNeedHands() {
        return needHands;
    }

    public void setNeedHands(Boolean needHands) {
        this.needHands = needHands;
    }

    public Boolean isNeedGrooming() {
        return needGrooming;
    }

    public void setNeedGrooming(Boolean needGrooming) {
        this.needGrooming = needGrooming;
    }

    public Boolean isNeedMedHelp() {
        return needMedHelp;
    }

    public void setNeedMedHelp(Boolean needMedHelp) {
        this.needMedHelp = needMedHelp;
    }

    public Boolean isNeedWalk() {
        return needWalk;
    }

    public void setNeedWalk(Boolean needWalk) {
        this.needWalk = needWalk;
    }

    public Boolean isNeedOverexposure() {
        return needOverexposure;
    }

    public void setNeedOverexposure(Boolean needOverexposure) {
        this.needOverexposure = needOverexposure;
    }

    public Boolean isNeedGroupDesignHelp() {
        return needGroupDesignHelp;
    }

    public void setNeedGroupDesignHelp(Boolean needGroupDesignHelp) {
        this.needGroupDesignHelp = needGroupDesignHelp;
    }

    public Boolean isNeedRepair() {
        return needRepair;
    }

    public void setNeedRepair(Boolean needRepair) {
        this.needRepair = needRepair;
    }

    public Boolean isNeedTransport() {
        return needTransport;
    }

    public void setNeedTransport(Boolean needTransport) {
        this.needTransport = needTransport;
    }

    public Boolean isCanPhotoSession() {
        return canPhotoSession;
    }

    public void setCanPhotoSession(Boolean canPhotoSession) {
        this.canPhotoSession = canPhotoSession;
    }

    public Boolean isCanVisit() {
        return canVisit;
    }

    public void setCanVisit(Boolean canVisit) {
        this.canVisit = canVisit;
    }

    public String getLoadAlbums() {
        return loadAlbums;
    }

    public void setLoadAlbums(String loadAlbums) {
        this.loadAlbums = loadAlbums;
    }

    public String getLoadPhotoFromAlbums() {
        return loadPhotoFromAlbums;
    }

    public void setLoadPhotoFromAlbums(String loadPhotoFromAlbums) {
        this.loadPhotoFromAlbums = loadPhotoFromAlbums;
    }

    public Boolean isLoadByTags() {
        return loadByTags;
    }

    public void setLoadByTags(Boolean loadByTags) {
        this.loadByTags = loadByTags;
    }

    public String getPayLink() {
        return payLink;
    }

    public void setPayLink(String payLink) {
        this.payLink = payLink;
    }

    public String getChatLink() {
        return chatLink;
    }

    public void setChatLink(String chatLink) {
        this.chatLink = chatLink;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShelterDTO shelterDTO = (ShelterDTO) o;
        if (shelterDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shelterDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShelterDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", link='" + getLink() + "'" +
            ", groupId=" + getGroupId() +
            ", city='" + getCity() + "'" +
            ", description='" + getDescription() + "'" +
            ", regInfo='" + getRegInfo() + "'" +
            ", needFeed='" + isNeedFeed() + "'" +
            ", needHands='" + isNeedHands() + "'" +
            ", needGrooming='" + isNeedGrooming() + "'" +
            ", needMedHelp='" + isNeedMedHelp() + "'" +
            ", needWalk='" + isNeedWalk() + "'" +
            ", needOverexposure='" + isNeedOverexposure() + "'" +
            ", needGroupDesignHelp='" + isNeedGroupDesignHelp() + "'" +
            ", needRepair='" + isNeedRepair() + "'" +
            ", needTransport='" + isNeedTransport() + "'" +
            ", canPhotoSession='" + isCanPhotoSession() + "'" +
            ", canVisit='" + isCanVisit() + "'" +
            ", loadAlbums='" + getLoadAlbums() + "'" +
            ", loadPhotoFromAlbums='" + getLoadPhotoFromAlbums() + "'" +
            ", loadByTags='" + isLoadByTags() + "'" +
            "}";
    }
}
