package ru.petthepet.server.config.vk;

import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.ServiceActor;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class VkConfig {

    @Getter private VkApiClient vk;
    @Getter private ServiceActor serviceActor;
            private VkProperties vkProperties;

    @Autowired
    public VkConfig(VkProperties vkProperties) {
        this.vkProperties = vkProperties;
    }

    @PostConstruct
    private void init() {
        TransportClient transportClient = new HttpTransportClient();
        VkApiClient vk = new VkApiClient(transportClient);
        ServiceActor actor = new ServiceActor(vkProperties.getClientId(),
                vkProperties.getProtectedKey(), vkProperties.getServiceAccessKey());
        this.vk = vk;
        this.serviceActor = actor;
    }

}
