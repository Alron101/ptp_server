package ru.petthepet.server.config.vk;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:config/vk.yaml")
@Configuration
@Data
public class VkProperties {

    @Value("${client_id}")
    private Integer clientId;
    @Value("${protected_key}")
    private String protectedKey;
    @Value("${service_access_key}")
    private String serviceAccessKey;

}
