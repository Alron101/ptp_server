package ru.petthepet.server.web.rest.vk;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.petthepet.server.service.VkApiService;

@Controller
@AllArgsConstructor(onConstructor_ = @Autowired)
@RequestMapping("/api")
public class VkApiController {

    private VkApiService vkApiService;

    @GetMapping("/vk/load")
    @ResponseBody
    public String loadDataFromShelterGroups() {
        String result;
        try {
            vkApiService.loadDataFromShelterGroups();
            result = "{ \"status\": \"OK\" }";
        } catch (Exception e) {
            result = "{ \"status\": \"ERROR\", \"message\": \"" + e.getMessage() + "\"}";
        }
        return result;
    }

}
