package ru.petthepet.server.web.rest.errors;

public class VkApiCallException extends Exception {

    public VkApiCallException() {
    }

    public VkApiCallException(String message) {
        super(message);
    }

    public VkApiCallException(String message, Throwable cause) {
        super(message, cause);
    }

    public VkApiCallException(Throwable cause) {
        super(cause);
    }

    public VkApiCallException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
