/**
 * View Models used by Spring MVC REST controllers.
 */
package ru.petthepet.server.web.rest.vm;
