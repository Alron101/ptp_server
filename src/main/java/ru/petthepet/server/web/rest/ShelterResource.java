package ru.petthepet.server.web.rest;

import ru.petthepet.server.service.ShelterService;
import ru.petthepet.server.web.rest.errors.BadRequestAlertException;
import ru.petthepet.server.service.dto.ShelterDTO;
import ru.petthepet.server.service.dto.ShelterCriteria;
import ru.petthepet.server.service.ShelterQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link ru.petthepet.server.domain.Shelter}.
 */
@RestController
@RequestMapping("/api")
public class ShelterResource {

    private final Logger log = LoggerFactory.getLogger(ShelterResource.class);

    private static final String ENTITY_NAME = "shelter";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShelterService shelterService;

    private final ShelterQueryService shelterQueryService;

    public ShelterResource(ShelterService shelterService, ShelterQueryService shelterQueryService) {
        this.shelterService = shelterService;
        this.shelterQueryService = shelterQueryService;
    }

    /**
     * {@code POST  /shelters} : Create a new shelter.
     *
     * @param shelterDTO the shelterDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shelterDTO, or with status {@code 400 (Bad Request)} if the shelter has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shelters")
    public ResponseEntity<ShelterDTO> createShelter(@Valid @RequestBody ShelterDTO shelterDTO) throws URISyntaxException {
        log.debug("REST request to save Shelter : {}", shelterDTO);
        if (shelterDTO.getId() != null) {
            throw new BadRequestAlertException("A new shelter cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShelterDTO result = shelterService.save(shelterDTO);
        return ResponseEntity.created(new URI("/api/shelters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shelters} : Updates an existing shelter.
     *
     * @param shelterDTO the shelterDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shelterDTO,
     * or with status {@code 400 (Bad Request)} if the shelterDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shelterDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shelters")
    public ResponseEntity<ShelterDTO> updateShelter(@Valid @RequestBody ShelterDTO shelterDTO) throws URISyntaxException {
        log.debug("REST request to update Shelter : {}", shelterDTO);
        if (shelterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShelterDTO result = shelterService.save(shelterDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shelterDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shelters} : get all the shelters.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shelters in body.
     */
    @GetMapping("/shelters")
    public ResponseEntity<List<ShelterDTO>> getAllShelters(ShelterCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Shelters by criteria: {}", criteria);
        Page<ShelterDTO> page = shelterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /shelters/count} : count all the shelters.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/shelters/count")
    public ResponseEntity<Long> countShelters(ShelterCriteria criteria) {
        log.debug("REST request to count Shelters by criteria: {}", criteria);
        return ResponseEntity.ok().body(shelterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /shelters/:id} : get the "id" shelter.
     *
     * @param id the id of the shelterDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shelterDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shelters/{id}")
    public ResponseEntity<ShelterDTO> getShelter(@PathVariable Long id) {
        log.debug("REST request to get Shelter : {}", id);
        Optional<ShelterDTO> shelterDTO = shelterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shelterDTO);
    }

    /**
     * {@code DELETE  /shelters/:id} : delete the "id" shelter.
     *
     * @param id the id of the shelterDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shelters/{id}")
    public ResponseEntity<Void> deleteShelter(@PathVariable Long id) {
        log.debug("REST request to delete Shelter : {}", id);
        shelterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/shelters?query=:query} : search for the shelter corresponding
     * to the query.
     *
     * @param query the query of the shelter search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/shelters")
    public ResponseEntity<List<ShelterDTO>> searchShelters(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Shelters for query {}", query);
        Page<ShelterDTO> page = shelterService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
