package ru.petthepet.server.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.hibernate.annotations.Formula;
import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Shelter.
 */
@Entity
@Table(name = "shelter")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "shelter")
public class Shelter implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "link")
    private String link;

    @Column(name = "logo")
    private String logo;

    @Column(name = "group_id")
    private Integer groupId;

    @Column(name = "city")
    private String city;

    @Size(max = 4096)
    @Column(name = "description", length = 4096)
    private String description;

    @Size(max = 4096)
    @Column(name = "reg_info", length = 4096)
    private String regInfo;

    @Column(name = "need_feed")
    private Boolean needFeed;

    @Column(name = "need_hands")
    private Boolean needHands;

    @Column(name = "need_grooming")
    private Boolean needGrooming;

    @Column(name = "need_med_help")
    private Boolean needMedHelp;

    @Column(name = "need_walk")
    private Boolean needWalk;

    @Column(name = "need_overexposure")
    private Boolean needOverexposure;

    @Column(name = "need_group_design_help")
    private Boolean needGroupDesignHelp;

    @Column(name = "need_repair")
    private Boolean needRepair;

    @Column(name = "need_transport")
    private Boolean needTransport;

    @Column(name = "can_photo_session")
    private Boolean canPhotoSession;

    @Column(name = "can_visit")
    private Boolean canVisit;

    @Size(max = 4096)
    @Column(name = "load_albums", length = 4096)
    private String loadAlbums;

    @Size(max = 4096)
    @Column(name = "load_photo_from_albums", length = 4096)
    private String loadPhotoFromAlbums;

    @Column(name = "load_by_tags")
    private Boolean loadByTags;

    @OneToMany(mappedBy = "shelter")
    private Set<Pet> pets = new HashSet<>();

    @Formula("concat(link,'?w=app5727453_',group_id)")
    private String payLink;

    @Formula("concat('https://vk.com/im?sel=',group_id)")
    private String chatLink;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Shelter name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public Shelter link(String link) {
        this.link = link;
        return this;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public Shelter groupId(Integer groupId) {
        this.groupId = groupId;
        return this;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getCity() {
        return city;
    }

    public Shelter city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDescription() {
        return description;
    }

    public Shelter description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRegInfo() {
        return regInfo;
    }

    public Shelter regInfo(String regInfo) {
        this.regInfo = regInfo;
        return this;
    }

    public void setRegInfo(String regInfo) {
        this.regInfo = regInfo;
    }

    public Boolean isNeedFeed() {
        return needFeed;
    }

    public Shelter needFeed(Boolean needFeed) {
        this.needFeed = needFeed;
        return this;
    }

    public void setNeedFeed(Boolean needFeed) {
        this.needFeed = needFeed;
    }

    public Boolean isNeedHands() {
        return needHands;
    }

    public Shelter needHands(Boolean needHands) {
        this.needHands = needHands;
        return this;
    }

    public void setNeedHands(Boolean needHands) {
        this.needHands = needHands;
    }

    public Boolean isNeedGrooming() {
        return needGrooming;
    }

    public Shelter needGrooming(Boolean needGrooming) {
        this.needGrooming = needGrooming;
        return this;
    }

    public void setNeedGrooming(Boolean needGrooming) {
        this.needGrooming = needGrooming;
    }

    public Boolean isNeedMedHelp() {
        return needMedHelp;
    }

    public Shelter needMedHelp(Boolean needMedHelp) {
        this.needMedHelp = needMedHelp;
        return this;
    }

    public void setNeedMedHelp(Boolean needMedHelp) {
        this.needMedHelp = needMedHelp;
    }

    public Boolean isNeedWalk() {
        return needWalk;
    }

    public Shelter needWalk(Boolean needWalk) {
        this.needWalk = needWalk;
        return this;
    }

    public void setNeedWalk(Boolean needWalk) {
        this.needWalk = needWalk;
    }

    public Boolean isNeedOverexposure() {
        return needOverexposure;
    }

    public Shelter needOverexposure(Boolean needOverexposure) {
        this.needOverexposure = needOverexposure;
        return this;
    }

    public void setNeedOverexposure(Boolean needOverexposure) {
        this.needOverexposure = needOverexposure;
    }

    public Boolean isNeedGroupDesignHelp() {
        return needGroupDesignHelp;
    }

    public Shelter needGroupDesignHelp(Boolean needGroupDesignHelp) {
        this.needGroupDesignHelp = needGroupDesignHelp;
        return this;
    }

    public void setNeedGroupDesignHelp(Boolean needGroupDesignHelp) {
        this.needGroupDesignHelp = needGroupDesignHelp;
    }

    public Boolean isNeedRepair() {
        return needRepair;
    }

    public Shelter needRepair(Boolean needRepair) {
        this.needRepair = needRepair;
        return this;
    }

    public void setNeedRepair(Boolean needRepair) {
        this.needRepair = needRepair;
    }

    public Boolean isNeedTransport() {
        return needTransport;
    }

    public Shelter needTransport(Boolean needTransport) {
        this.needTransport = needTransport;
        return this;
    }

    public void setNeedTransport(Boolean needTransport) {
        this.needTransport = needTransport;
    }

    public Boolean isCanPhotoSession() {
        return canPhotoSession;
    }

    public Shelter canPhotoSession(Boolean canPhotoSession) {
        this.canPhotoSession = canPhotoSession;
        return this;
    }

    public void setCanPhotoSession(Boolean canPhotoSession) {
        this.canPhotoSession = canPhotoSession;
    }

    public Boolean isCanVisit() {
        return canVisit;
    }

    public Shelter canVisit(Boolean canVisit) {
        this.canVisit = canVisit;
        return this;
    }

    public void setCanVisit(Boolean canVisit) {
        this.canVisit = canVisit;
    }

    public String getLoadAlbums() {
        return loadAlbums;
    }

    public Shelter loadAlbums(String loadAlbums) {
        this.loadAlbums = loadAlbums;
        return this;
    }

    public void setLoadAlbums(String loadAlbums) {
        this.loadAlbums = loadAlbums;
    }

    public String getLoadPhotoFromAlbums() {
        return loadPhotoFromAlbums;
    }

    public Shelter loadPhotoFromAlbums(String loadPhotoFromAlbums) {
        this.loadPhotoFromAlbums = loadPhotoFromAlbums;
        return this;
    }

    public void setLoadPhotoFromAlbums(String loadPhotoFromAlbums) {
        this.loadPhotoFromAlbums = loadPhotoFromAlbums;
    }

    public Boolean isLoadByTags() {
        return loadByTags;
    }

    public Shelter loadByTags(Boolean loadByTags) {
        this.loadByTags = loadByTags;
        return this;
    }

    public void setLoadByTags(Boolean loadByTags) {
        this.loadByTags = loadByTags;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public Shelter pets(Set<Pet> pets) {
        this.pets = pets;
        return this;
    }

    public Shelter addPet(Pet pet) {
        this.pets.add(pet);
        pet.setShelter(this);
        return this;
    }

    public Shelter removePet(Pet pet) {
        this.pets.remove(pet);
        pet.setShelter(null);
        return this;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public String getPayLink() {
        return payLink;
    }

    public String getChatLink() {
        return chatLink;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Shelter)) {
            return false;
        }
        return id != null && id.equals(((Shelter) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Shelter{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", link='" + getLink() + "'" +
            ", groupId=" + getGroupId() +
            ", city='" + getCity() + "'" +
            ", description='" + getDescription() + "'" +
            ", regInfo='" + getRegInfo() + "'" +
            ", needFeed='" + isNeedFeed() + "'" +
            ", needHands='" + isNeedHands() + "'" +
            ", needGrooming='" + isNeedGrooming() + "'" +
            ", needMedHelp='" + isNeedMedHelp() + "'" +
            ", needWalk='" + isNeedWalk() + "'" +
            ", needOverexposure='" + isNeedOverexposure() + "'" +
            ", needGroupDesignHelp='" + isNeedGroupDesignHelp() + "'" +
            ", needRepair='" + isNeedRepair() + "'" +
            ", needTransport='" + isNeedTransport() + "'" +
            ", canPhotoSession='" + isCanPhotoSession() + "'" +
            ", canVisit='" + isCanVisit() + "'" +
            ", loadAlbums='" + getLoadAlbums() + "'" +
            ", loadPhotoFromAlbums='" + getLoadPhotoFromAlbums() + "'" +
            ", loadByTags='" + isLoadByTags() + "'" +
            "}";
    }
}
