package ru.petthepet.server.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

/**
 * A Pet.
 */
@Entity
@Table(name = "pet")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "pet")
public class Pet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "sex")
    private Boolean sex;

    @Column(name = "age")
    private Integer age;

    @Column(name = "photo")
    private String photo;

    @Size(max = 4096)
    @Column(name = "description", length = 4096)
    private String description;

    @Column(name = "breed")
    private String breed;

    @Column(name = "genus")
    private String genus;

    @ManyToOne
    @JsonIgnoreProperties("pets")
    private Shelter shelter;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Pet name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isSex() {
        return sex;
    }

    public Pet sex(Boolean sex) {
        this.sex = sex;
        return this;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public Pet age(Integer age) {
        this.age = age;
        return this;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPhoto() {
        return photo;
    }

    public Pet photo(String photo) {
        this.photo = photo;
        return this;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescription() {
        return description;
    }

    public Pet description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBreed() {
        return breed;
    }

    public Pet breed(String breed) {
        this.breed = breed;
        return this;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getGenus() {
        return genus;
    }

    public Pet genus(String genus) {
        this.genus = genus;
        return this;
    }

    public void setGenus(String genus) {
        this.genus = genus;
    }

    public Shelter getShelter() {
        return shelter;
    }

    public Pet shelter(Shelter shelter) {
        this.shelter = shelter;
        return this;
    }

    public void setShelter(Shelter shelter) {
        this.shelter = shelter;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pet)) {
            return false;
        }
        return id != null && id.equals(((Pet) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Pet{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", sex='" + isSex() + "'" +
            ", age=" + getAge() +
            ", photo='" + getPhoto() + "'" +
            ", description='" + getDescription() + "'" +
            ", breed='" + getBreed() + "'" +
            ", genus='" + getGenus() + "'" +
            "}";
    }
}
