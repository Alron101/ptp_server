import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './shelter.reducer';
import { IShelter } from 'app/shared/model/shelter.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IShelterUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IShelterUpdateState {
  isNew: boolean;
}

export class ShelterUpdate extends React.Component<IShelterUpdateProps, IShelterUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (!this.state.isNew) {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { shelterEntity } = this.props;
      const entity = {
        ...shelterEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/shelter');
  };

  render() {
    const { shelterEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="ptpApp.shelter.home.createOrEditLabel">
              <Translate contentKey="ptpApp.shelter.home.createOrEditLabel">Create or edit a Shelter</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : shelterEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="shelter-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="shelter-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="nameLabel" for="shelter-name">
                    <Translate contentKey="ptpApp.shelter.name">Name</Translate>
                  </Label>
                  <AvField id="shelter-name" type="text" name="name" />
                </AvGroup>
                <AvGroup>
                  <Label id="linkLabel" for="shelter-link">
                    <Translate contentKey="ptpApp.shelter.link">Link</Translate>
                  </Label>
                  <AvField id="shelter-link" type="text" name="link" />
                </AvGroup>
                <AvGroup>
                  <Label id="groupIdLabel" for="shelter-groupId">
                    <Translate contentKey="ptpApp.shelter.groupId">Group Id</Translate>
                  </Label>
                  <AvField id="shelter-groupId" type="string" className="form-control" name="groupId" />
                </AvGroup>
                <AvGroup>
                  <Label id="cityLabel" for="shelter-city">
                    <Translate contentKey="ptpApp.shelter.city">City</Translate>
                  </Label>
                  <AvField id="shelter-city" type="text" name="city" />
                </AvGroup>
                <AvGroup>
                  <Label id="descriptionLabel" for="shelter-description">
                    <Translate contentKey="ptpApp.shelter.description">Description</Translate>
                  </Label>
                  <AvField
                    id="shelter-description"
                    type="text"
                    name="description"
                    validate={{
                      maxLength: { value: 4096, errorMessage: translate('entity.validation.maxlength', { max: 4096 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="regInfoLabel" for="shelter-regInfo">
                    <Translate contentKey="ptpApp.shelter.regInfo">Reg Info</Translate>
                  </Label>
                  <AvField
                    id="shelter-regInfo"
                    type="text"
                    name="regInfo"
                    validate={{
                      maxLength: { value: 4096, errorMessage: translate('entity.validation.maxlength', { max: 4096 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="needFeedLabel" check>
                    <AvInput id="shelter-needFeed" type="checkbox" className="form-control" name="needFeed" />
                    <Translate contentKey="ptpApp.shelter.needFeed">Need Feed</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="needHandsLabel" check>
                    <AvInput id="shelter-needHands" type="checkbox" className="form-control" name="needHands" />
                    <Translate contentKey="ptpApp.shelter.needHands">Need Hands</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="needGroomingLabel" check>
                    <AvInput id="shelter-needGrooming" type="checkbox" className="form-control" name="needGrooming" />
                    <Translate contentKey="ptpApp.shelter.needGrooming">Need Grooming</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="needMedHelpLabel" check>
                    <AvInput id="shelter-needMedHelp" type="checkbox" className="form-control" name="needMedHelp" />
                    <Translate contentKey="ptpApp.shelter.needMedHelp">Need Med Help</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="needWalkLabel" check>
                    <AvInput id="shelter-needWalk" type="checkbox" className="form-control" name="needWalk" />
                    <Translate contentKey="ptpApp.shelter.needWalk">Need Walk</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="needOverexposureLabel" check>
                    <AvInput id="shelter-needOverexposure" type="checkbox" className="form-control" name="needOverexposure" />
                    <Translate contentKey="ptpApp.shelter.needOverexposure">Need Overexposure</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="needGroupDesignHelpLabel" check>
                    <AvInput id="shelter-needGroupDesignHelp" type="checkbox" className="form-control" name="needGroupDesignHelp" />
                    <Translate contentKey="ptpApp.shelter.needGroupDesignHelp">Need Group Design Help</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="needRepairLabel" check>
                    <AvInput id="shelter-needRepair" type="checkbox" className="form-control" name="needRepair" />
                    <Translate contentKey="ptpApp.shelter.needRepair">Need Repair</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="needTransportLabel" check>
                    <AvInput id="shelter-needTransport" type="checkbox" className="form-control" name="needTransport" />
                    <Translate contentKey="ptpApp.shelter.needTransport">Need Transport</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="canPhotoSessionLabel" check>
                    <AvInput id="shelter-canPhotoSession" type="checkbox" className="form-control" name="canPhotoSession" />
                    <Translate contentKey="ptpApp.shelter.canPhotoSession">Can Photo Session</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="canVisitLabel" check>
                    <AvInput id="shelter-canVisit" type="checkbox" className="form-control" name="canVisit" />
                    <Translate contentKey="ptpApp.shelter.canVisit">Can Visit</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="loadAlbumsLabel" for="shelter-loadAlbums">
                    <Translate contentKey="ptpApp.shelter.loadAlbums">Load Albums</Translate>
                  </Label>
                  <AvField
                    id="shelter-loadAlbums"
                    type="text"
                    name="loadAlbums"
                    validate={{
                      maxLength: { value: 4096, errorMessage: translate('entity.validation.maxlength', { max: 4096 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="loadPhotoFromAlbumsLabel" for="shelter-loadPhotoFromAlbums">
                    <Translate contentKey="ptpApp.shelter.loadPhotoFromAlbums">Load Photo From Albums</Translate>
                  </Label>
                  <AvField
                    id="shelter-loadPhotoFromAlbums"
                    type="text"
                    name="loadPhotoFromAlbums"
                    validate={{
                      maxLength: { value: 4096, errorMessage: translate('entity.validation.maxlength', { max: 4096 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="loadByTagsLabel" check>
                    <AvInput id="shelter-loadByTags" type="checkbox" className="form-control" name="loadByTags" />
                    <Translate contentKey="ptpApp.shelter.loadByTags">Load By Tags</Translate>
                  </Label>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/shelter" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  shelterEntity: storeState.shelter.entity,
  loading: storeState.shelter.loading,
  updating: storeState.shelter.updating,
  updateSuccess: storeState.shelter.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShelterUpdate);
