import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './shelter.reducer';
import { IShelter } from 'app/shared/model/shelter.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IShelterDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ShelterDetail extends React.Component<IShelterDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { shelterEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="ptpApp.shelter.detail.title">Shelter</Translate> [<b>{shelterEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="name">
                <Translate contentKey="ptpApp.shelter.name">Name</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.name}</dd>
            <dt>
              <span id="link">
                <Translate contentKey="ptpApp.shelter.link">Link</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.link}</dd>
            <dt>
              <span id="groupId">
                <Translate contentKey="ptpApp.shelter.groupId">Group Id</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.groupId}</dd>
            <dt>
              <span id="city">
                <Translate contentKey="ptpApp.shelter.city">City</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.city}</dd>
            <dt>
              <span id="description">
                <Translate contentKey="ptpApp.shelter.description">Description</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.description}</dd>
            <dt>
              <span id="regInfo">
                <Translate contentKey="ptpApp.shelter.regInfo">Reg Info</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.regInfo}</dd>
            <dt>
              <span id="needFeed">
                <Translate contentKey="ptpApp.shelter.needFeed">Need Feed</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.needFeed ? 'true' : 'false'}</dd>
            <dt>
              <span id="needHands">
                <Translate contentKey="ptpApp.shelter.needHands">Need Hands</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.needHands ? 'true' : 'false'}</dd>
            <dt>
              <span id="needGrooming">
                <Translate contentKey="ptpApp.shelter.needGrooming">Need Grooming</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.needGrooming ? 'true' : 'false'}</dd>
            <dt>
              <span id="needMedHelp">
                <Translate contentKey="ptpApp.shelter.needMedHelp">Need Med Help</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.needMedHelp ? 'true' : 'false'}</dd>
            <dt>
              <span id="needWalk">
                <Translate contentKey="ptpApp.shelter.needWalk">Need Walk</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.needWalk ? 'true' : 'false'}</dd>
            <dt>
              <span id="needOverexposure">
                <Translate contentKey="ptpApp.shelter.needOverexposure">Need Overexposure</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.needOverexposure ? 'true' : 'false'}</dd>
            <dt>
              <span id="needGroupDesignHelp">
                <Translate contentKey="ptpApp.shelter.needGroupDesignHelp">Need Group Design Help</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.needGroupDesignHelp ? 'true' : 'false'}</dd>
            <dt>
              <span id="needRepair">
                <Translate contentKey="ptpApp.shelter.needRepair">Need Repair</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.needRepair ? 'true' : 'false'}</dd>
            <dt>
              <span id="needTransport">
                <Translate contentKey="ptpApp.shelter.needTransport">Need Transport</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.needTransport ? 'true' : 'false'}</dd>
            <dt>
              <span id="canPhotoSession">
                <Translate contentKey="ptpApp.shelter.canPhotoSession">Can Photo Session</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.canPhotoSession ? 'true' : 'false'}</dd>
            <dt>
              <span id="canVisit">
                <Translate contentKey="ptpApp.shelter.canVisit">Can Visit</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.canVisit ? 'true' : 'false'}</dd>
            <dt>
              <span id="loadAlbums">
                <Translate contentKey="ptpApp.shelter.loadAlbums">Load Albums</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.loadAlbums}</dd>
            <dt>
              <span id="loadPhotoFromAlbums">
                <Translate contentKey="ptpApp.shelter.loadPhotoFromAlbums">Load Photo From Albums</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.loadPhotoFromAlbums}</dd>
            <dt>
              <span id="loadByTags">
                <Translate contentKey="ptpApp.shelter.loadByTags">Load By Tags</Translate>
              </span>
            </dt>
            <dd>{shelterEntity.loadByTags ? 'true' : 'false'}</dd>
          </dl>
          <Button tag={Link} to="/entity/shelter" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/shelter/${shelterEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ shelter }: IRootState) => ({
  shelterEntity: shelter.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShelterDetail);
