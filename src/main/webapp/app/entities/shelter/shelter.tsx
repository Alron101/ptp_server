import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, InputGroup, Col, Row, Table } from 'reactstrap';
import { AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudSearchAction, ICrudGetAllAction, getSortState, IPaginationBaseState } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getSearchEntities, getEntities, reset } from './shelter.reducer';
import { IShelter } from 'app/shared/model/shelter.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IShelterProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export interface IShelterState extends IPaginationBaseState {
  search: string;
}

export class Shelter extends React.Component<IShelterProps, IShelterState> {
  state: IShelterState = {
    search: '',
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.reset();
  }

  componentDidUpdate() {
    if (this.props.updateSuccess) {
      this.reset();
    }
  }

  search = () => {
    if (this.state.search) {
      this.props.reset();
      this.setState({ activePage: 1 }, () => {
        const { activePage, itemsPerPage, sort, order, search } = this.state;
        this.props.getSearchEntities(search, activePage - 1, itemsPerPage, `${sort},${order}`);
      });
    }
  };

  clear = () => {
    this.props.reset();
    this.setState({ search: '', activePage: 1 }, () => {
      this.props.getEntities();
    });
  };

  handleSearch = event => this.setState({ search: event.target.value });

  reset = () => {
    this.props.reset();
    this.setState({ activePage: 1 }, () => {
      this.getEntities();
    });
  };

  handleLoadMore = () => {
    if (window.pageYOffset > 0) {
      this.setState({ activePage: this.state.activePage + 1 }, () => this.getEntities());
    }
  };

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => {
        this.reset();
      }
    );
  };

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order, search } = this.state;
    if (search) {
      this.props.getSearchEntities(search, activePage - 1, itemsPerPage, `${sort},${order}`);
    } else {
      this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
    }
  };

  render() {
    const { shelterList, match } = this.props;
    return (
      <div>
        <h2 id="shelter-heading">
          <Translate contentKey="ptpApp.shelter.home.title">Shelters</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="ptpApp.shelter.home.createLabel">Create a new Shelter</Translate>
          </Link>
        </h2>
        <Row>
          <Col sm="12">
            <AvForm onSubmit={this.search}>
              <AvGroup>
                <InputGroup>
                  <AvInput
                    type="text"
                    name="search"
                    value={this.state.search}
                    onChange={this.handleSearch}
                    placeholder={translate('ptpApp.shelter.home.search')}
                  />
                  <Button className="input-group-addon">
                    <FontAwesomeIcon icon="search" />
                  </Button>
                  <Button type="reset" className="input-group-addon" onClick={this.clear}>
                    <FontAwesomeIcon icon="trash" />
                  </Button>
                </InputGroup>
              </AvGroup>
            </AvForm>
          </Col>
        </Row>
        <div className="table-responsive">
          <InfiniteScroll
            pageStart={this.state.activePage}
            loadMore={this.handleLoadMore}
            hasMore={this.state.activePage - 1 < this.props.links.next}
            loader={<div className="loader">Loading ...</div>}
            threshold={0}
            initialLoad={false}
          >
            {shelterList && shelterList.length > 0 ? (
              <Table responsive aria-describedby="shelter-heading">
                <thead>
                  <tr>
                    <th className="hand" onClick={this.sort('id')}>
                      <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('name')}>
                      <Translate contentKey="ptpApp.shelter.name">Name</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('link')}>
                      <Translate contentKey="ptpApp.shelter.link">Link</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('groupId')}>
                      <Translate contentKey="ptpApp.shelter.groupId">Group Id</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('city')}>
                      <Translate contentKey="ptpApp.shelter.city">City</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('description')}>
                      <Translate contentKey="ptpApp.shelter.description">Description</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('regInfo')}>
                      <Translate contentKey="ptpApp.shelter.regInfo">Reg Info</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('needFeed')}>
                      <Translate contentKey="ptpApp.shelter.needFeed">Need Feed</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('needHands')}>
                      <Translate contentKey="ptpApp.shelter.needHands">Need Hands</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('needGrooming')}>
                      <Translate contentKey="ptpApp.shelter.needGrooming">Need Grooming</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('needMedHelp')}>
                      <Translate contentKey="ptpApp.shelter.needMedHelp">Need Med Help</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('needWalk')}>
                      <Translate contentKey="ptpApp.shelter.needWalk">Need Walk</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('needOverexposure')}>
                      <Translate contentKey="ptpApp.shelter.needOverexposure">Need Overexposure</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('needGroupDesignHelp')}>
                      <Translate contentKey="ptpApp.shelter.needGroupDesignHelp">Need Group Design Help</Translate>{' '}
                      <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('needRepair')}>
                      <Translate contentKey="ptpApp.shelter.needRepair">Need Repair</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('needTransport')}>
                      <Translate contentKey="ptpApp.shelter.needTransport">Need Transport</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('canPhotoSession')}>
                      <Translate contentKey="ptpApp.shelter.canPhotoSession">Can Photo Session</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('canVisit')}>
                      <Translate contentKey="ptpApp.shelter.canVisit">Can Visit</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('loadAlbums')}>
                      <Translate contentKey="ptpApp.shelter.loadAlbums">Load Albums</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('loadPhotoFromAlbums')}>
                      <Translate contentKey="ptpApp.shelter.loadPhotoFromAlbums">Load Photo From Albums</Translate>{' '}
                      <FontAwesomeIcon icon="sort" />
                    </th>
                    <th className="hand" onClick={this.sort('loadByTags')}>
                      <Translate contentKey="ptpApp.shelter.loadByTags">Load By Tags</Translate> <FontAwesomeIcon icon="sort" />
                    </th>
                    <th />
                  </tr>
                </thead>
                <tbody>
                  {shelterList.map((shelter, i) => (
                    <tr key={`entity-${i}`}>
                      <td>
                        <Button tag={Link} to={`${match.url}/${shelter.id}`} color="link" size="sm">
                          {shelter.id}
                        </Button>
                      </td>
                      <td>{shelter.name}</td>
                      <td>{shelter.link}</td>
                      <td>{shelter.groupId}</td>
                      <td>{shelter.city}</td>
                      <td>{shelter.description}</td>
                      <td>{shelter.regInfo}</td>
                      <td>{shelter.needFeed ? 'true' : 'false'}</td>
                      <td>{shelter.needHands ? 'true' : 'false'}</td>
                      <td>{shelter.needGrooming ? 'true' : 'false'}</td>
                      <td>{shelter.needMedHelp ? 'true' : 'false'}</td>
                      <td>{shelter.needWalk ? 'true' : 'false'}</td>
                      <td>{shelter.needOverexposure ? 'true' : 'false'}</td>
                      <td>{shelter.needGroupDesignHelp ? 'true' : 'false'}</td>
                      <td>{shelter.needRepair ? 'true' : 'false'}</td>
                      <td>{shelter.needTransport ? 'true' : 'false'}</td>
                      <td>{shelter.canPhotoSession ? 'true' : 'false'}</td>
                      <td>{shelter.canVisit ? 'true' : 'false'}</td>
                      <td>{shelter.loadAlbums}</td>
                      <td>{shelter.loadPhotoFromAlbums}</td>
                      <td>{shelter.loadByTags ? 'true' : 'false'}</td>
                      <td className="text-right">
                        <div className="btn-group flex-btn-group-container">
                          <Button tag={Link} to={`${match.url}/${shelter.id}`} color="info" size="sm">
                            <FontAwesomeIcon icon="eye" />{' '}
                            <span className="d-none d-md-inline">
                              <Translate contentKey="entity.action.view">View</Translate>
                            </span>
                          </Button>
                          <Button tag={Link} to={`${match.url}/${shelter.id}/edit`} color="primary" size="sm">
                            <FontAwesomeIcon icon="pencil-alt" />{' '}
                            <span className="d-none d-md-inline">
                              <Translate contentKey="entity.action.edit">Edit</Translate>
                            </span>
                          </Button>
                          <Button tag={Link} to={`${match.url}/${shelter.id}/delete`} color="danger" size="sm">
                            <FontAwesomeIcon icon="trash" />{' '}
                            <span className="d-none d-md-inline">
                              <Translate contentKey="entity.action.delete">Delete</Translate>
                            </span>
                          </Button>
                        </div>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            ) : (
              <div className="alert alert-warning">
                <Translate contentKey="ptpApp.shelter.home.notFound">No Shelters found</Translate>
              </div>
            )}
          </InfiniteScroll>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ shelter }: IRootState) => ({
  shelterList: shelter.entities,
  totalItems: shelter.totalItems,
  links: shelter.links,
  entity: shelter.entity,
  updateSuccess: shelter.updateSuccess
});

const mapDispatchToProps = {
  getSearchEntities,
  getEntities,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Shelter);
