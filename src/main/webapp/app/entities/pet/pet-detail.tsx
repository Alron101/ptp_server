import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './pet.reducer';
import { IPet } from 'app/shared/model/pet.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPetDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class PetDetail extends React.Component<IPetDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { petEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="ptpApp.pet.detail.title">Pet</Translate> [<b>{petEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="name">
                <Translate contentKey="ptpApp.pet.name">Name</Translate>
              </span>
            </dt>
            <dd>{petEntity.name}</dd>
            <dt>
              <span id="sex">
                <Translate contentKey="ptpApp.pet.sex">Sex</Translate>
              </span>
            </dt>
            <dd>{petEntity.sex ? 'true' : 'false'}</dd>
            <dt>
              <span id="age">
                <Translate contentKey="ptpApp.pet.age">Age</Translate>
              </span>
            </dt>
            <dd>{petEntity.age}</dd>
            <dt>
              <span id="photo">
                <Translate contentKey="ptpApp.pet.photo">Photo</Translate>
              </span>
            </dt>
            <dd>{petEntity.photo}</dd>
            <dt>
              <span id="description">
                <Translate contentKey="ptpApp.pet.description">Description</Translate>
              </span>
            </dt>
            <dd>{petEntity.description}</dd>
            <dt>
              <span id="breed">
                <Translate contentKey="ptpApp.pet.breed">Breed</Translate>
              </span>
            </dt>
            <dd>{petEntity.breed}</dd>
            <dt>
              <span id="genus">
                <Translate contentKey="ptpApp.pet.genus">Genus</Translate>
              </span>
            </dt>
            <dd>{petEntity.genus}</dd>
            <dt>
              <Translate contentKey="ptpApp.pet.shelter">Shelter</Translate>
            </dt>
            <dd>{petEntity.shelterName ? petEntity.shelterName : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/pet" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/pet/${petEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ pet }: IRootState) => ({
  petEntity: pet.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PetDetail);
