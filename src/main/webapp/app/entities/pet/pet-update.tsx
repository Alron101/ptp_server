import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IShelter } from 'app/shared/model/shelter.model';
import { getEntities as getShelters } from 'app/entities/shelter/shelter.reducer';
import { getEntity, updateEntity, createEntity, reset } from './pet.reducer';
import { IPet } from 'app/shared/model/pet.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPetUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IPetUpdateState {
  isNew: boolean;
  shelterId: string;
}

export class PetUpdate extends React.Component<IPetUpdateProps, IPetUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      shelterId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (!this.state.isNew) {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getShelters();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { petEntity } = this.props;
      const entity = {
        ...petEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/pet');
  };

  render() {
    const { petEntity, shelters, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="ptpApp.pet.home.createOrEditLabel">
              <Translate contentKey="ptpApp.pet.home.createOrEditLabel">Create or edit a Pet</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : petEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="pet-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="pet-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="nameLabel" for="pet-name">
                    <Translate contentKey="ptpApp.pet.name">Name</Translate>
                  </Label>
                  <AvField id="pet-name" type="text" name="name" />
                </AvGroup>
                <AvGroup>
                  <Label id="sexLabel" check>
                    <AvInput id="pet-sex" type="checkbox" className="form-control" name="sex" />
                    <Translate contentKey="ptpApp.pet.sex">Sex</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="ageLabel" for="pet-age">
                    <Translate contentKey="ptpApp.pet.age">Age</Translate>
                  </Label>
                  <AvField id="pet-age" type="string" className="form-control" name="age" />
                </AvGroup>
                <AvGroup>
                  <Label id="photoLabel" for="pet-photo">
                    <Translate contentKey="ptpApp.pet.photo">Photo</Translate>
                  </Label>
                  <AvField id="pet-photo" type="text" name="photo" />
                </AvGroup>
                <AvGroup>
                  <Label id="descriptionLabel" for="pet-description">
                    <Translate contentKey="ptpApp.pet.description">Description</Translate>
                  </Label>
                  <AvField
                    id="pet-description"
                    type="text"
                    name="description"
                    validate={{
                      maxLength: { value: 4096, errorMessage: translate('entity.validation.maxlength', { max: 4096 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="breedLabel" for="pet-breed">
                    <Translate contentKey="ptpApp.pet.breed">Breed</Translate>
                  </Label>
                  <AvField id="pet-breed" type="text" name="breed" />
                </AvGroup>
                <AvGroup>
                  <Label id="genusLabel" for="pet-genus">
                    <Translate contentKey="ptpApp.pet.genus">Genus</Translate>
                  </Label>
                  <AvField id="pet-genus" type="text" name="genus" />
                </AvGroup>
                <AvGroup>
                  <Label for="pet-shelter">
                    <Translate contentKey="ptpApp.pet.shelter">Shelter</Translate>
                  </Label>
                  <AvInput id="pet-shelter" type="select" className="form-control" name="shelterId">
                    <option value="" key="0" />
                    {shelters
                      ? shelters.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.name}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/pet" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  shelters: storeState.shelter.entities,
  petEntity: storeState.pet.entity,
  loading: storeState.pet.loading,
  updating: storeState.pet.updating,
  updateSuccess: storeState.pet.updateSuccess
});

const mapDispatchToProps = {
  getShelters,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PetUpdate);
