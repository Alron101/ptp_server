import { IPet } from 'app/shared/model/pet.model';

export interface IShelter {
  id?: number;
  name?: string;
  link?: string;
  groupId?: number;
  city?: string;
  description?: string;
  regInfo?: string;
  needFeed?: boolean;
  needHands?: boolean;
  needGrooming?: boolean;
  needMedHelp?: boolean;
  needWalk?: boolean;
  needOverexposure?: boolean;
  needGroupDesignHelp?: boolean;
  needRepair?: boolean;
  needTransport?: boolean;
  canPhotoSession?: boolean;
  canVisit?: boolean;
  loadAlbums?: string;
  loadPhotoFromAlbums?: string;
  loadByTags?: boolean;
  pets?: IPet[];
}

export const defaultValue: Readonly<IShelter> = {
  needFeed: false,
  needHands: false,
  needGrooming: false,
  needMedHelp: false,
  needWalk: false,
  needOverexposure: false,
  needGroupDesignHelp: false,
  needRepair: false,
  needTransport: false,
  canPhotoSession: false,
  canVisit: false,
  loadByTags: false
};
