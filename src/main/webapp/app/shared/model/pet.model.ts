export interface IPet {
  id?: number;
  name?: string;
  sex?: boolean;
  age?: number;
  photo?: string;
  description?: string;
  breed?: string;
  genus?: string;
  shelterName?: string;
  shelterId?: number;
}

export const defaultValue: Readonly<IPet> = {
  sex: false
};
